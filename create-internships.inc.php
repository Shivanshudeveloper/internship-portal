<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
        include './includes/user-navigation.inc.php';
	?>
<div id="internship-section">
	<!-- Body Comes Here -->
	<section class="feature-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="section-title text-center">
						<h1>
							<i class="fas fa-briefcase"></i>
                             Internships
                        </h1>
					</div>
				</div>
			</div>
			<div class="feature-inner row">
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="50" height="50"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g><g id="surface1"><path d="M17.91667,35.83333h118.25v118.25h-118.25z" fill="#90caf9"></path><path d="M152.93555,29.17057l-10.10612,-10.09212c-1.53971,-1.53972 -4.03125,-1.53972 -5.57096,0l-4.75911,4.74512l15.67708,15.67708l4.75911,-4.75911c1.52572,-1.52572 1.52572,-4.03125 0,-5.57097" fill="#e57373"></path><path d="M140.33789,47.35319l-65.63379,65.61979l-15.67708,-15.67708l65.61979,-65.61979z" fill="#ff9800"></path><path d="M124.66081,31.67611l7.82454,-7.83854l15.69108,15.67708l-7.83854,7.83854z" fill="#b0bec5"></path><path d="M59.02702,97.2959l-5.27702,20.9541l20.9541,-5.27702z" fill="#ffc107"></path><path d="M56.40951,107.65397l-2.65951,10.59603l10.59603,-2.6595z" fill="#37474f"></path></g></g></g></svg>
						<h4>Create New Internships</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
							<p>
							</p>
						</div>
						<a class="btn btn-primary btn-sm float-right" id="createNewInternships" href="#">Create Now</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="50" height="50"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g><g id="surface1"><path d="M28.66667,10.75h114.66667v150.5h-114.66667z" fill="#90caf9"></path><path d="M78.83333,75.25h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M78.83333,53.75h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M78.83333,64.5h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M78.83333,86h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M78.83333,32.25h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M78.83333,10.75h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M78.83333,21.5h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M78.83333,43h14.33333v7.16667h-14.33333z" fill="#1976d2"></path><path d="M93.16667,96.75h-14.33333c0,10.75 -7.16667,21.5 -7.16667,28.66667c0,7.92253 6.41081,14.33333 14.33333,14.33333c7.92253,0 14.33333,-6.41081 14.33333,-14.33333c0,-7.16667 -7.16667,-17.91667 -7.16667,-28.66667zM86,132.58333c-3.96126,0 -7.16667,-3.2054 -7.16667,-7.16667c0,-3.96126 3.2054,-7.16667 7.16667,-7.16667c3.96126,0 7.16667,3.2054 7.16667,7.16667c0,3.96126 -3.2054,7.16667 -7.16667,7.16667z" fill="#3f51b5"></path></g></g></g></svg>
						<h4>Archives</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
							<p>
							</p>
						</div>
						<a class="btn btn-primary btn-sm float-right" href="#">Show</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="50" height="50"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#2ecc71"><g id="surface1"><path d="M30.96,65.36c-11.38156,0 -20.64,9.25844 -20.64,20.64c0,11.38156 9.25844,20.64 20.64,20.64c11.38156,0 20.64,-9.25844 20.64,-20.64c0,-11.38156 -9.25844,-20.64 -20.64,-20.64zM86,65.36c-11.38156,0 -20.64,9.25844 -20.64,20.64c0,11.38156 9.25844,20.64 20.64,20.64c11.38156,0 20.64,-9.25844 20.64,-20.64c0,-11.38156 -9.25844,-20.64 -20.64,-20.64zM141.04,65.36c-11.38156,0 -20.64,9.25844 -20.64,20.64c0,11.38156 9.25844,20.64 20.64,20.64c11.38156,0 20.64,-9.25844 20.64,-20.64c0,-11.38156 -9.25844,-20.64 -20.64,-20.64z"></path></g></g></g></svg>
						<h4>Find More</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
							<p>
							</p>
						</div>
						<a class="btn btn-primary btn-sm float-right" href="#">Explore</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Body Comes Here -->
</div>
	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>