$(document).ready(() => {
    

    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            console.log("User Login")
        } else {
            console.log("No User Login")
        }
    })
    var pageURL = window.location.href;
        url = new URL(pageURL);
        page = url.searchParams.get("page");
        type = url.searchParams.get("type");
    if (page === "dashboard") {
        firebase.auth().onAuthStateChanged(firebaseUser => {
            if (firebaseUser) {
                $("#userName").html(`<a class="text-success" href="#">${firebaseUser.email}</a>`)
            } else {
                console.log("No User Login")
                window.location.href = "login.php";
            }
        })
        // Checking for the dashboard type
        if (type === "student") {
            $("#root").load("./dashboards/student.php")
            $("#dashboardHome").html(`<a class="text-success" href="./dashboard.php?type=student&page=dashboard">Home</a>`)
            $("#internshipDetails").html(`<a class="text-success" onclick="internshipAppliedPage()" href="#">Internships Applied</a>`)
            $("#internshipMore").html(`<a class="text-success" href="#">Internships in Popular Cities</a>`)
        } else if (type === "govermentdepartment") {
            $("#root").load("./dashboards/goverment-department.php")
            $("#dashboardHome").html(`<a class="text-success" href="./dashboard.php?type=govermentdepartment&page=dashboard">Work Panel</a>`)
        }
    }
    // Getting the page name
    var path = window.location.pathname;
    var pageName = path.split("/").pop();
    if (pageName === "overview.inc.php") {
        console.log(sessionStorage.getItem("userEmail"))
    }
})

$("#createNewInternships").click(() => {
    $("#internship-section").load("./components/createNewInternships.inc.php")
})

// For Logout
$("#logout").click(() => {
    firebase.auth().signOut().then(function () {
        window.location.href = "index.php"
    }).catch(function (error) {
        console.error(error)
    });
})

// For Login
$("#login-btn").click(() => {
    var email = $("#email-text").val()
        password = $("#password-pwd").val()

    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage)
    });
    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            if (firebaseUser.email === false) {
                window.location.href = "email.php";
            } else {
                sessionStorage.setItem("page", "dashboard")
                sessionStorage.setItem("userEmail", firebaseUser.email)
                window.location.href = `check.php?check=user&email=${firebaseUser.email}`;
            }
        } else {
            console.log("No User Login")
        }
    })
})

// View Internship For Organization
const viewInternship = (id) => {
    window.location.href = `view-internships-details.inc.php?type=govermentdepartment&page=dashboard&internshipId=${id}`
}

// For Goverment Registration
$("#submit-govermentSector-btn").click(() => {
    var firstName = $("#GovfirstName-txt").val()
        lastName = $("#GovlastName-txt").val()
        email = $("#Govemail-txt").val()
        organization = $("#GovOrganization-txt").val()
        pwd = $("#Govpassword-pwd").val()
        if (firstName === "" || lastName === "" || email === "" || organization === "" || pwd === "") {
            swal("Empty Fields", `You left a field empty`, "warning")
        } else {
            if (IsEmail(email)==false){
                swal("Error", `Not a valid Email!`, "error")
            } else {
                $.post("./src/php/main.php", {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    organization: organization,
                    pwd: pwd,
                    usertype: 'GOVERMENT ORGANIZATION',
                    govOrganizationRegister: true
                }).then(() => {
                    $("#allFormsContainer").html('<h3 class="text-center mb-30">Please Wait Loading....</h3> <img src="./img/loading/loading.gif" alt="loading" /> ')
                    firebase.auth().createUserWithEmailAndPassword(email, pwd)
                        .then(() => {
                            console.log("User Created")
                            var user = firebase.auth().currentUser;
                            user.sendEmailVerification().then(function() {
                                swal("Registered",`A verification mail is send to ${email}`, "success", {
                                    button: "Okay",
                                })
                                .then((value) => {
                                    // User Email Added to session
                                    sessionStorage.setItem("emailRegister", email);
                                    window.location.href = "email.php?register=Goverment Organization";
                                });
                            }).catch(function(error) {
                                swal("Error", `${error}`, "error")
                            });
                        })
                        .catch((err) => swal("Error", `${err}`, "error"))
                })
                .catch((err) => swal("Error", err, "error") )
            }
        }
})

// For Student Registration
$("#student-register-btn").click(() => {
    var firstName = $("#StuFirstName-txt").val()
        lastName = $("#StuLastName-txt").val()
        email = $("#StuEmail-txt").val()
        institute = $("#myInput").val()
        program = $("#StuSelectProgram").val()
        firstInternship = $("#internshipFields-firstPreference-select").val()
        secondInternship = $("#internshipFields-secondPreference-select").val()
        thirdInternship = $("#internshipFields-thirdPreference-select").val()
        type = $("#Stutype-select").val()
        state = $("#StuSelectState").val()
        pwd = $("#Stu-pwd").val()
        // Checking for empty field
        if (firstName === "" || lastName === "" || email === "" || institute === "" || program === "" || firstInternship === "" || secondInternship === "" || thirdInternship === "" || type === "" || state === "" || pwd === "") {
            swal("Empty Fields", `You left a field empty`, "warning")
        } else {
            if (IsEmail(email)==false){
                swal("Error", `Not a valid Email!`, "error")
            } else {
                $.post("./src/php/main.php", {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    institute: institute,
                    program: program,
                    firstInternship: firstInternship,
                    secondInternship: secondInternship,
                    thirdInternship: thirdInternship,
                    type: type,
                    state: state,
                    password: pwd,
                    usertype: 'STUDENT',
                    studentRegister: true
                })
                $("#allFormsContainer").html('<h3 class="text-center mb-30">Please Wait Loading....</h3> <img src="./img/loading/loading.gif" alt="loading" />')
                firebase.auth().createUserWithEmailAndPassword(email, pwd)
                    .then(() => {
                        console.log("User Created")
                        var user = firebase.auth().currentUser;
                        user.sendEmailVerification().then(function() {
                            swal("Registered",`A verification mail is send to ${email}`, "success", {
                                button: "Okay",
                            })
                            .then((value) => {
                                sessionStorage.setItem("emailRegister", email);
                                window.location.href = "email.php?register=student";
                            });
                          }).catch(function(error) {
                            swal("Error", `${error}`, "error")
                          });
                    })
                    .catch((err) => swal("Error", `${err}`, "error"))
            }
        }

})

// For Corporate Sector
$("#submit-corporateSector-btn").click(() => {
    var firstName = $("#firstNameCorporateSector-txt").val()
        lastName = $("#lastNameCorporateSector-txt").val()
        email = $("#emailCorporateSector-txt").val()
        select = $("#select-CorporateSector").val()
        id = $("#idNumberCorporateSector-txt").val()
        organization = $("#organizationCorporateSector-txt").val()
        pwd = $("#password-CorporateSector-pwd").val()
        // Checking for empty field
        if (firstName === "" || lastName === "" || email === "" || select === "" || id === "" || organization === "" || pwd === "" ) {
            swal("Empty Fields", `You left a field empty`, "warning")
        } else {
            if (IsEmail(email)==false){
                swal("Error", `Not a valid Email!`, "error")
            } else {
                $.post("./src/php/main.php", {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    select: select,
                    id: id,
                    organization: organization,
                    corporateRegister: true
                })
                $("#allFormsContainer").html('<h3 class="text-center mb-30">Please Wait Loading....</h3> <img src="./img/loading/loading.gif" alt="loading" />')
                firebase.auth().createUserWithEmailAndPassword(email, pwd)
                    .then(() => {
                        console.log("User Created")
                        var user = firebase.auth().currentUser;
                        user.sendEmailVerification().then(function() {
                            swal("Registered",`A verification mail is send to ${email}`, "success", {
                                button: "Okay",
                            })
                            .then((value) => {
                                sessionStorage.setItem("emailRegister", email);
                                window.location.href = "email.php?register=corporate";
                            });
                          }).catch(function(error) {
                            swal("Error", `${error}`, "error")
                          });
                    })
                    .catch((err) => swal("Error", `${err}`, "error"))
            }
        }
})

// File Upload
// Goverment Department logo Upload
$("#fileButton").change((e) => {
    $("#submit-govermentDepartment-info-btn").attr("disabled", true)
    $("#submit-govermentDepartment-info-btn").html('Uploading...')
    $("#submit-govermentDepartment-info-btn").attr('class', 'btn btn-block btn-secondary')
    var email = $("#govDepartmentEmail").val()
    var file = e.target.files[0];
    // Create a storage ref
    var uid = create_UUID()
    var storageRef = firebase.storage().ref('pictures/Goverment_Department/' + uid +"-" +file.name);
        fileName = uid +"-" +file.name
    // Upload File
    var task = storageRef.put(file);
    task.on('state_changed',
        function progress(snapshot) {
            var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            uploader.value = percentage;
        },
        function error(err) {
            swal("Error", `${err}`, "error")
        },
        function complete() {
            storageRef.getDownloadURL().then((url) => {
                $.post("./src/php/main.php", {
                    email: email,
                    logo: url,
                    govDepartmentLogoUpload: true
                }).then(() => {
                    $("#submit-govermentDepartment-info-btn").attr("disabled", false)
                    $("#submit-govermentDepartment-info-btn").html('Continue')
                    $("#submit-govermentDepartment-info-btn").attr('class', 'btn btn-block btn-primary')
                })
            })
        }
    );
})
// Apply for the Internship
$("#apply-forInternship-btn").click(() => {
    swal({
        title: "Are you sure?",
        text: "You want to apply for this Internship",
        icon: "warning",
        buttons: true,
        dangerMode: false,
      })
      .then((willDelete) => {
        if (willDelete) {
            var userEmail = sessionStorage.getItem("userEmail")
                internshipId = $("#internshipId").val()
            $.post("./src/php/main.php", {
                email: userEmail,
                internshipId: internshipId,
                applyForInternshipStudent: true
            }).then(() => {
                swal({
                    title: "Good job!",
                    text: "You have Successfully applied for internship!",
                    icon: "success",
                    button: "Aww yiss!",
                  });
            })
        } else {

        }
      });
})

// Applied For Internship Page
const internshipAppliedPage = () => {
    var userEmail = sessionStorage.getItem("userEmail")
    window.location.href = `internship-student-applied.php?type=student&page=dashboard&email=${userEmail}`
}

// Cancel Internship Request
const cancel_StudentInternship = (id) => {
    swal({
        title: "Are you sure?",
        text: "You want to request to cancel internship",
        icon: "warning",
        buttons: true,
        dangerMode: false,
      })
      .then((willDelete) => {
        if (willDelete) {
            var userEmail = sessionStorage.getItem("userEmail")
            $.post("./src/php/main.php", {
                userEmail: userEmail,
                internshipId: id,
                cancelInternship: true
            }).then(() => {
                swal({
                    title: "Internship Cancel",
                    text: "We have cancel you request for this Internship.",
                    icon: "success",
                    button: "Okay",
                  }).then(() => {
                      location.reload()
                  })
            })
        } else {

        }
      });
}

// Coporate Sector Link
const coporateSectorLink = () => {
    $("#allInternships").load("./src/php/main.php", {
        corporateSectorLink: true
    })
}

// Govermental Sector Link
const govermentalSectorLink = () => {
    $("#allInternships").load("./src/php/main.php", {
        govermentalSectorLink: true
    })
}

// Institute Sector Link
const instituteSectorLink = () => {
    $("#allInternships").load("./src/php/main.php", {
        instituteSectorLink: true
    })
}

// Aggregator Sector Link
const aggregatorSectorLink = () => {
    $("#allInternships").load("./src/php/main.php", {
        aggregatorSectorLink: true
    })
}

// Mark Internship Post
const markInternshipPost = (id) => {
    $.notify({
        // options
        message: 'Bookmark Added' 
    },{
        // settings
        type: 'success'
    });
    var userEmail = sessionStorage.getItem("userEmail")
    $.post("./src/php/main.php", {
        email: userEmail,
        internshipId: id,
        addtoFavorite: true
    })
}

// Edit Post
$("#edit-Post").click(() => {
    var postId = $("#postId").val()
    $("#editInternshipPost").load("./components/editInternshipPost.inc.php", {
        postId: postId
    })
})

// Generating UID
function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

// Get the page name
function pageName() {
    var path = window.location.pathname;
    var page = path.split("/").pop();
    return page    
}



// Email Validation
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
      return false;
    }else{
      return true;
    }
}