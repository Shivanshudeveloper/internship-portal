<?php
include './dbh.php';

// Login Handler
if (isset($_POST['loginIn'])) {
    session_start();
    $_SESSION['login'] = 1;
    $_SESSION['email'] = mysqli_real_escape_string($conn, $_POST['email']);
}

// Adding Phone Number Goverment Department
if (isset($_POST['phoneNumberVerifyGovOrganization'])) {
    $phoneNumber = mysqli_real_escape_string($conn, $_POST['phoneNumber']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $sql = "UPDATE gov_organization SET phone = '$phoneNumber' WHERE email = '$email';";
    mysqli_query($conn, $sql);
}

// Addding Phone Number Corporate
if (isset($_POST['phoneNumberVerifyStudent'])) {
    $phoneNumber = mysqli_real_escape_string($conn, $_POST['phoneNumber']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $sql = "UPDATE student SET phone = '$phoneNumber' WHERE email = '$email';";
    mysqli_query($conn, $sql);
}
// Adding Phone Number Student
if (isset($_POST['phoneNumberVerifyCorporate'])) {
    $phoneNumber = mysqli_real_escape_string($conn, $_POST['phoneNumber']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $sql = "UPDATE corporate SET phone = '$phoneNumber' WHERE email = '$email';";
    mysqli_query($conn, $sql);
}

// For Corporate Registration
if (isset($_POST['corporateRegister'])) {
    $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
    $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $select = mysqli_real_escape_string($conn, $_POST['select']);
    $id = mysqli_real_escape_string($conn, $_POST['id']);
    $organization = mysqli_real_escape_string($conn, $_POST['organization']);
    $sql = "INSERT INTO corporate(firstName, lastName, email, verification, organization) VALUES('$firstName', '$lastName', '$email', '$select', '$organization');";
    mysqli_query($conn, $sql);
}

// For Student Registration
if (isset($_POST['studentRegister'])) {
    $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
    $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $institute = mysqli_real_escape_string($conn, $_POST['institute']);
    $pwd = mysqli_real_escape_string($conn, $_POST['password']);
    $userType = mysqli_real_escape_string($conn, $_POST['usertype']);
    $hash_pwd = password_hash($pwd, PASSWORD_DEFAULT);
    $uid = "STU".uniqid().time();
    $currentTimeinSeconds = time(); 
    $registerOn = date('Y-m-d', $currentTimeinSeconds);
    $sql = "INSERT INTO student(uid, first_name, last_name, email, institution, register_on, first_time, password) VALUES('$uid', '$firstName', '$lastName', '$email', '$institute', '$registerOn', 1, '$hash_pwd');";
    mysqli_query($conn, $sql);
    $sql = "INSERT INTO users(uid, type, email) VALUES('$uid', '$userType', '$email');";
    mysqli_query($conn, $sql);
}

// Student Intrest
if (isset($_POST['submit-student-interest-btn'])) {
    $program = mysqli_real_escape_string($conn, $_POST['StuSelectProgram']);
    $firstInternship = mysqli_real_escape_string($conn, $_POST['internshipFields-firstPreference-select']);
    $secondInternship = mysqli_real_escape_string($conn, $_POST['internshipFields-secondPreference-select']);
    $thirdInternship = mysqli_real_escape_string($conn, $_POST['internshipFields-thirdPreference-select']);
    $type = mysqli_real_escape_string($conn, $_POST['Stutype-select']);
    $state = mysqli_real_escape_string($conn, $_POST['StuSelectState']);
    $email = mysqli_real_escape_string($conn, $_POST['studentEmail']);
    $skypeID = mysqli_real_escape_string($conn, $_POST['skypeId']);
    $sql = "UPDATE student SET program = '$program', first_internship = '$firstInternship', second_internship = '$secondInternship', third_internship = '$thirdInternship', type = '$type', skype_id = '$skypeID', first_time = 0 WHERE email = '$email';";
    mysqli_query($conn, $sql);
    header("Location: ../../dashboard.php?page=dashboard&type=student&email=".$email);
}

// Goverment Department Information
if (isset($_POST['submit-govermentDepartment-info-btn'])) {
    $skypeId = mysqli_real_escape_string($conn, $_POST['skypeId']);
    $address = mysqli_real_escape_string($conn, $_POST['address']);
    $email = mysqli_real_escape_string($conn, $_POST['govDepartmentEmail']);
    $sql = "UPDATE gov_organization SET skype_id = '$skypeId', address = '$address', first_time = 0 WHERE email = '$email';";
    mysqli_query($conn, $sql);
    header("Location: ../../dashboard.php?page=dashboard&email=".$email);
}


// Goverment Department
if (isset($_POST['govOrganizationRegister'])) {
    $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
    $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $organization = mysqli_real_escape_string($conn, $_POST['organization']);
    $pwd = mysqli_real_escape_string($conn, $_POST['email']);
    $userType = mysqli_real_escape_string($conn, $_POST['usertype']);
    $hash_pwd = password_hash($pwd, PASSWORD_DEFAULT);
    // Generate UID
    $uid = "GOVDEP".uniqid().time();
    $currentTimeinSeconds = time(); 
    $registerOn = date('Y-m-d', $currentTimeinSeconds);
    $sql = "INSERT INTO gov_organization (uid ,first_name, last_name, email, organization, register_on, first_time, password) VALUES ('$uid', '$firstName', '$lastName', '$email', '$organization', '$registerOn', 1, '$hash_pwd');";
    mysqli_query($conn, $sql);
    $sql = "INSERT INTO users(uid, type, email) VALUES('$uid', '$userType', '$email');";
    mysqli_query($conn, $sql);
}

// Goverment Department Logo Upload
if (isset($_POST['govDepartmentLogoUpload'])) {
    $fileName = mysqli_real_escape_string($conn, $_POST['logo']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $sql = "UPDATE gov_organization SET logo = '$fileName' WHERE email = '$email';";
    mysqli_query($conn, $sql);
}

// Post Internship
if (isset($_GET['postInternship-btn'])) {
    $currentTimeinSeconds = time(); 
    $postInternshipOn = date('Y-m-d', $currentTimeinSeconds);
    // Generate UID
    $uid = "INTERNSHIP".uniqid().time();
    $analyticsUid = "ANALYTICS".uniqid().time();
    $dataValues = array();
    $internshipCategory = array();
    foreach ($_GET as $key => $value) {
        $dataInput = mysqli_real_escape_string($conn, $_GET[$key]);
        if (endsWith($key, "-chk")) {
            array_push($internshipCategory, $key);
        } else {
            array_push($dataValues, $value);
        }
    }
    $internCategory = '';
    foreach($internshipCategory as $value) {
        if (empty($internCategory)) {
            $internCategory = $value.$internCategory;
        } else {
            $internCategory = $internCategory.', '.$value;
        }
    }
    $title = '';
    $duration = '';
    $studentsYear = '';
    $stipend = '';
    $start_date = '';
    $end_date = '';
    $number_seats = '';
    $description = '';
    for ($i = 0; $i < 8; $i++) {
        if ($i === 0) {
            $title = $dataValues[$i];
        } else if ($i === 1) {
            $duration = $dataValues[$i];
        } else if ($i === 2) {
            $studentsYear = $dataValues[$i];
        } else if ($i === 3) {
            $stipend = $dataValues[$i];
        } else if ($i === 4) {
            $start_date = $dataValues[$i];
        } else if ($i === 5) {
            $end_date = $dataValues[$i];
        } else if ($i === 6) {
            $number_seats = $dataValues[$i];
        } else if ($i === 7) {
            $description = $dataValues[$i];
        } 
    }
    session_start();
    $useremail = $_SESSION['useremail'];
    $sql = "INSERT INTO internships_analytics (uid, internship_uid, internship_of, total_applied) VALUES('$analyticsUid', '$uid', '$useremail', 0)";    
    mysqli_query($conn, $sql);
    $sql = "INSERT INTO internships(uid, internship_of, title, maximum_duration, stipend,start_date, end_date, internship_criterial, number_seats, students_year, description, post_on) VALUES('$uid', '$useremail', '$title', '$duration', '$stipend', '$start_date', '$end_date', '$internCategory', '$number_seats', '$studentsYear', '$description', '$postInternshipOn')";
    $res = mysqli_query($conn, $sql);
    if ($res) {
        header("Location: ../../dashboard.php?type=govermentdepartment&page=dashboard&NewMsg=InternshipPostedSuccess");
    } else {
        echo "Server Error";
    }
}

// Update Internship Post
if (isset($_POST['updateInternshipPost-btn'])) {
    $postId = mysqli_real_escape_string($conn, $_POST['postId']);
    $title = mysqli_real_escape_string($conn, $_POST['internship-title']);
    $stipend = mysqli_real_escape_string($conn, $_POST['stipend-internship']);
    $startDate = mysqli_real_escape_string($conn, $_POST['startDate-txt']);
    $endDate = mysqli_real_escape_string($conn, $_POST['endDate-txt']);
    $numberSeats = mysqli_real_escape_string($conn, $_POST['numberSeats-txt']);
    $description = mysqli_real_escape_string($conn, $_POST['internship-description']);
    $sql = "UPDATE internships SET title = '$title', stipend = '$stipend', start_date = '$startDate', end_date = '$endDate', number_seats = '$numberSeats', description = '$description' WHERE id = '$postId';";
    $res = mysqli_query($conn, $sql);
    if ($res) {
        header("Location: ../../dashboard.php?type=govermentdepartment&page=dashboard&NewMsg=InternshipPostedSuccess");
    } else {
        echo "Server Error";
    }
}

// Applied For Internship
if (isset($_POST['applyForInternshipStudent'])) {
    $internshipId = mysqli_real_escape_string($conn, $_POST['internshipId']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $internshipUid = '';
    $sql = "SELECT * FROM internships WHERE id = '$internshipId';";
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
        $internshipUid = $row['uid'];
    }
    $uid = "APPLIED".uniqid().time();
    $currentTimeinSeconds = time(); 
    $appliedInternshipOn = date('Y-m-d', $currentTimeinSeconds);
    $sql = "SELECT * FROM internship_applied WHERE user_email = '$email' AND internship_id = '$internshipId';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        $sql = "SELECT * FROM internships_analytics WHERE internship_uid = '$internshipUid';";
        $result = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_assoc($result)) {
            $internshipCount = $row['total_applied'];
        }
        $internshipCount = (int)$internshipCount;
        $internshipCount = $internshipCount + 1;
        $sql = "UPDATE internships_analytics SET total_applied = '$internshipCount' WHERE internship_uid = '$internshipUid';";
        mysqli_query($conn, $sql);
        $sql = "INSERT INTO internship_applied(uid, internship_id, user_email, applied_on) VALUES ('$uid', '$internshipId', '$email', '$appliedInternshipOn');";
        mysqli_query($conn, $sql);
    } 
}
// Corporate Sector Link
if (isset($_POST['corporateSectorLink'])) {
    $sql = "SELECT * FROM `internships` WHERE `sector`='CORPORATE';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        echo "<h3>No Internship Found</h3>";
    } else {
        while ($row = mysqli_fetch_assoc($result)) {
            echo '
            <div class="card mb-3">
                <div class="card-body">
                    <form action="overview-student.inc.php?type=student&page=dashboard" method="POST">
                        <input type="hidden" name="id" value='.$row['id'].'>
                        <h5 class="card-title">'.$row['title'].'</h5>
                        <h6 class="card-title"><i style="color: green;" class="fas fa-money-bill-wave-alt"></i>  '.$row['stipend'].'</h6>
                        <p class="card-text">'.$row['description'].'</p>
                        <button class="btn btn-sm btn-primary">View</button>
                    </form>
                </div>
            </div>
            ';
        }
    }
}

// Govermental Sector Link
if (isset($_POST['govermentalSectorLink'])) {
    $sql = "SELECT * FROM `internships` WHERE `sector`='GOVERMENTAL';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        echo "<h3>No Internship Found</h3>";
    } else {
        while ($row = mysqli_fetch_assoc($result)) {
            echo '
            <div class="card mb-3">
                <div class="card-body">
                    <form action="overview-student.inc.php?type=student&page=dashboard" method="POST">
                        <input type="hidden" name="id" value='.$row['id'].'>
                        <h5 class="card-title">'.$row['title'].'</h5>
                        <h6 class="card-title"><i style="color: green;" class="fas fa-money-bill-wave-alt"></i>  '.$row['stipend'].'</h6>
                        <p class="card-text">'.$row['description'].'</p>
                        <button class="btn btn-sm btn-primary">View</button>
                    </form>
                </div>
            </div>
            ';
        }
    }
}

// Institute Sector Link
if (isset($_POST['instituteSectorLink'])) {
    $sql = "SELECT * FROM `internships` WHERE `sector`='INSTITUTE';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        echo "<h3>No Internship Found</h3>";
    } else {
        while ($row = mysqli_fetch_assoc($result)) {
            echo '
            <div class="card mb-3">
                <div class="card-body">
                    <form action="overview-student.inc.php?type=student&page=dashboard" method="POST">
                        <input type="hidden" name="id" value='.$row['id'].'>
                        <h5 class="card-title">'.$row['title'].'</h5>
                        <h6 class="card-title"><i style="color: green;" class="fas fa-money-bill-wave-alt"></i>  '.$row['stipend'].'</h6>
                        <p class="card-text">'.$row['description'].'</p>
                        <button class="btn btn-sm btn-primary">View</button>
                    </form>
                </div>
            </div>
            ';
        }
    }
}

// Aggregator Sector Link
if (isset($_POST['aggregatorSectorLink'])) {
    $sql = "SELECT * FROM `internships` WHERE `sector`='AGGREGATOR';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        echo "<h3>No Internship Found</h3>";
    } else {
        while ($row = mysqli_fetch_assoc($result)) {
            echo '
            <div class="card mb-3">
                <div class="card-body">
                    <form action="overview-student.inc.php?type=student&page=dashboard" method="POST">
                        <input type="hidden" name="id" value='.$row['id'].'>
                        <h5 class="card-title">'.$row['title'].'</h5>
                        <h6 class="card-title"><i style="color: green;" class="fas fa-money-bill-wave-alt"></i>  '.$row['stipend'].'</h6>
                        <p class="card-text">'.$row['description'].'</p>
                        <button class="btn btn-sm btn-primary">View</button>
                    </form>
                </div>
            </div>
            ';
        }
    }
}

// Cancel User Internship
if (isset($_POST['cancelInternship'])) {
    $internshipId = mysqli_real_escape_string($conn, $_POST['internshipId']);
    $userEmail = mysqli_real_escape_string($conn, $_POST['userEmail']);
    $sql = "DELETE FROM internship_applied WHERE internship_id = '$internshipId' AND user_email = '$userEmail';";
    mysqli_query($conn, $sql);
}

// Add to Favorite
if (isset($_POST['addtoFavorite'])) {
    $internshipId = mysqli_real_escape_string($conn, $_POST['internshipId']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $uid = "BOOKMARK".uniqid().time();
    $currentTimeinSeconds = time(); 
    $markInternshipOn = date('Y-m-d', $currentTimeinSeconds);
    $sql = "SELECT * FROM bookmark_internship WHERE email = '$email' AND internship_id = '$internshipId';";
    $result = mysqli_query($conn, $sql);
    $resultChk = mysqli_num_rows($result);
    if ($resultChk < 1) {
        $sql = "INSERT INTO bookmark_internship (uid, mark_for, internship_id, mark_on) VALUES ('$uid', '$email', '$internshipId', '$markInternshipOn');";
        mysqli_query($conn, $sql);
    } 
}

// Function for Ends With
function endsWith($string, $endString) { 
    $len = strlen($endString); 
    if ($len == 0) { 
        return true; 
    } 
    return (substr($string, -$len) === $endString); 
} 
?>