<section id="form">
    <section class="feature-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="section-title text-center">
						<h1>
                            <i class="fas fa-plus"></i>
                             Post Internship
                        </h1>
					</div>
				</div>
            </div>
        </div>
        <div class="w-50 container">
            <form action="./src/php/main.php" method="GET">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="internship-title" id="internship-title" placeholder="Enter Title For Internship" required>
            </div>
            <div class="form-group">
                <label for="title">Max. Durations (Months)</label>
                <select class="custom-select" name="max-durationMonths">
                    <option value="1 Month">1 Month</option>
                    <option value="2 Month">2 Month</option>
                    <option value="3 Month">3 Month</option>
                    <option value="4 Month">4 Month</option>
                    <option value="5 Month">5 Month</option>
                    <option value="6 Month">6 Month</option>
                    <option value="12 Month">12 Month</option>
                    <option value="24 Month">24 Month</option>
                    <option value="36 Month">36 Month</option>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Students of Year</label>
                <select class="custom-select" name="student-ofYear">
                    <option value="1 Year">1st</option>
                    <option value="2 Year">2nd</option>
                    <option value="3 Year">3rd</option>
                    <option value="4 Year">4th</option>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Stipend</label>
                <input type="number" min="1" class="form-control" name="stipend-internship" id="stipend-internship" placeholder="10000" required >
            </div>
            <div class="form-group">
                <label for="title">Start Date</label>
                <input type="date" class="form-control" name="startDate-txt" id="startDate-internship" required >
            </div>
            <div class="form-group">
                <label for="title">End Date</label>
                <input type="date" class="form-control" name="endDate-txt" id="endDate-internship" required >
            </div>
            <div class="form-group">
            <label for="title">Internship Criteria</label>
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <i class="fas fa-poll-h"></i>
                            Select Internship Criteria
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Architecture-chk" id="Architecture-chk">
                            <label class="form-check-label" for="exampleCheck1">Architecture</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="InteriorDesign-chk" id="InteriorDesign-chk">
                            <label class="form-check-label" for="exampleCheck1">Interior Design</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Commerce-chk" id="Commerce-chk">
                            <label class="form-check-label" for="exampleCheck1">Commerce</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Accounts-chk" id="Accounts-chk">
                            <label class="form-check-label" for="exampleCheck1">Accounts</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="CharteredAccountancy-chk" id="CharteredAccountancy-chk">
                            <label class="form-check-label" for="exampleCheck1">Chartered Accountancy</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Design-chk" id="Design-chk">
                            <label class="form-check-label" for="exampleCheck1">Design</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="FashionDesign-chk" id="FashionDesign-chk">
                            <label class="form-check-label" for="exampleCheck1">Fashion Design</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="GraphicDesign-chk" id="GraphicDesign-chk">
                            <label class="form-check-label" for="exampleCheck1">Graphic Design</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MerchandiseDesign-chk" id="MerchandiseDesign-chk">
                            <label class="form-check-label" for="exampleCheck1">Merchandise Design</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Animation-chk" id="Animation-chk">
                            <label class="form-check-label" for="exampleCheck1">Animation</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="AerospaceEngineering-chk" id="AerospaceEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Aerospace Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="BiotechnologyEngineering-chk" id="BiotechnologyEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Biotechnology Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="ChemicalEngineering-chk" id="ChemicalEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Chemical Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="CivilEngineering-chk" id="CivilEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Civil Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="ComputerVision-chk" id="ComputerVision-chk">
                            <label class="form-check-label" for="exampleCheck1">Computer Vision</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="ElectricalEngineering-chk" id="ElectricalEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Electrical Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="ElectronicsEngineering-chk" id="ElectronicsEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Electronics Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="EnergyScienceEngineering-chk" id="EnergyScienceEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Energy Science &amp; Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="EngineeringDesign-chk" id="EngineeringDesign-chk">
                            <label class="form-check-label" for="exampleCheck1">Engineering Design</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="EngineeringPhysics-chk" id="EngineeringPhysics-chk">
                            <label class="form-check-label" for="exampleCheck1">Engineering Physics</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Engineering-chk" id="Engineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="GameDevelopment-chk" id="GameDevelopment-chk">
                            <label class="form-check-label" for="exampleCheck1">Game Development</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MaterialScience-chk" id="MaterialScience-chk">
                            <label class="form-check-label" for="exampleCheck1">Material Science</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MechanicalEngineering-chk" id="MechanicalEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Mechanical Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MobileAppDevelopment-chk" id="MobileAppDevelopment-chk">
                            <label class="form-check-label" for="exampleCheck1">Mobile App Development</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="NavalArchitectureandOceanEngineeering-chk" id="NavalArchitectureandOceanEngineeering-chk">
                            <label class="form-check-label" for="exampleCheck1">Naval Architecture and Ocean Engineeering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="NetworkEngineering-chk" id="NetworkEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Network Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="PetroleumEngineering-chk" id="PetroleumEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Petroleum Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Programming-chk" id="Programming-chk">
                            <label class="form-check-label" for="exampleCheck1">Programming</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="SoftwareDevelopment-chk" id="SoftwareDevelopment-chk">
                            <label class="form-check-label" for="exampleCheck1">Software Development</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="SoftwareTesting-chk" id="SoftwareTesting-chk">
                            <label class="form-check-label" for="exampleCheck1">Software Testing</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="WebDevelopment-chk" id="WebDevelopment-chk">
                            <label class="form-check-label" for="exampleCheck1">Web Development</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Hospitality-chk" id="Hospitality-chk">
                            <label class="form-check-label" for="exampleCheck1">Hospitality</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="HotelManagement-chk" id="HotelManagement-chk">
                            <label class="form-check-label" for="exampleCheck1">Hotel Management</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="TravelTourism-chk" id="TravelTourism-chk">
                            <label class="form-check-label" for="exampleCheck1">Travel &amp; Tourism</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MBA-chk" id="MBA-chk">
                            <label class="form-check-label" for="exampleCheck1">MBA</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="DataEntry-chk" id="DataEntry-chk">
                            <label class="form-check-label" for="exampleCheck1">Data Entry</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="DigitalMarketing-chk" id="DigitalMarketing-chk">
                            <label class="form-check-label" for="exampleCheck1">Digital Marketing</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Finance-chk" id="Finance-chk">
                            <label class="form-check-label" for="exampleCheck1">Finance</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="GeneralManagement-chk" id="GeneralManagement-chk">
                            <label class="form-check-label" for="exampleCheck1">General Management</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="HumanResources-chk" id="HumanResources-chk">
                            <label class="form-check-label" for="exampleCheck1">Human Resources (HR)</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MarketBusinessResearch-chk" id="MarketBusinessResearch-chk">
                            <label class="form-check-label" for="exampleCheck1">Market/Business Research</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Marketing-chk" id="Marketing-chk">
                            <label class="form-check-label" for="exampleCheck1">Marketing</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Operations-chk" id="Operations-chk">
                            <label class="form-check-label" for="exampleCheck1">Operations</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MetallurgicalEngineering-chk" id="MetallurgicalEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Metallurgical</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Sales-chk" id="Sales-chk">
                            <label class="form-check-label" for="exampleCheck1">Sales</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Media-chk" id="Media-chk">
                            <label class="form-check-label" for="exampleCheck1">Media</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Cinematography-chk" id="Cinematography-chk">
                            <label class="form-check-label" for="exampleCheck1">Cinematography</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="ContentWriting-chk" id="ContentWriting-chk">
                            <label class="form-check-label" for="exampleCheck1">Content Writing</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="FilmMaking-chk" id="FilmMaking-chk">
                            <label class="form-check-label" for="exampleCheck1">Film Making</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Journalism-chk" id="Journalism-chk">
                            <label class="form-check-label" for="exampleCheck1">Journalism</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MotionGraphics-chk" id="MotionGraphics-chk">
                            <label class="form-check-label" for="exampleCheck1">Motion Graphics</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Photography-chk" id="Photography-chk">
                            <label class="form-check-label" for="exampleCheck1">Photography</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="PublicRelations-chk" id="PublicRelations-chk">
                            <label class="form-check-label" for="exampleCheck1">Public Relations (PR)</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="SocialMediaMarketing-chk" id="SocialMediaMarketing-chk">
                            <label class="form-check-label" for="exampleCheck1">Social Media Marketing</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="VideoMakingEditing-chk" id="VideoMakingEditing-chk">
                            <label class="form-check-label" for="exampleCheck1">Video Making/Editing</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MetallurgicalEngineering-chk" id="MetallurgicalEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Metallurgical</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Videography-chk" id="Videography-chk">
                            <label class="form-check-label" for="exampleCheck1">Videography</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="MetallurgicalEngineering-chk" id="MetallurgicalEngineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Metallurgical</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Science-chk" id="Science-chk">
                            <label class="form-check-label" for="exampleCheck1">Science</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Biology-chk" id="Biology-chk">
                            <label class="form-check-label" for="exampleCheck1">Biology</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Chemistry-chk" id="Chemistry-chk">
                            <label class="form-check-label" for="exampleCheck1">Chemistry</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Mathematics-chk" id="Mathematics-chk">
                            <label class="form-check-label" for="exampleCheck1">Mathematics</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Physics-chk" id="Physics-chk">
                            <label class="form-check-label" for="exampleCheck1">Physics</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Statistics-chk" id="Statistics-chk">
                            <label class="form-check-label" for="exampleCheck1">Statistics</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="AgricultureFood Engineering-chk" id="AgricultureFood Engineering-chk">
                            <label class="form-check-label" for="exampleCheck1">Agriculture &amp; Food Engineering</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="CampusAmbassador-chk" id="CampusAmbassador-chk">
                            <label class="form-check-label" for="exampleCheck1">Campus Ambassador</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="CompanySecretary-chk" id="CompanySecretary-chk">
                            <label class="form-check-label" for="exampleCheck1">Company Secretary (CS)</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="DataScience-chk" id="DataScience-chk">
                            <label class="form-check-label" for="exampleCheck1">Data Science</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="EventManagement-chk" id="EventManagement-chk">
                            <label class="form-check-label" for="exampleCheck1">EventManagement</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Humanities-chk" id="Humanities-chk">
                            <label class="form-check-label" for="exampleCheck1">Humanities</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Law-chk" id="Law-chk">
                            <label class="form-check-label" for="exampleCheck1">Law</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Medicine-chk" id="Medicine-chk">
                            <label class="form-check-label" for="exampleCheck1">Medicine</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Pharmaceutical-chk" id="Pharmaceutical-chk">
                            <label class="form-check-label" for="exampleCheck1">Pharmaceutical</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Teaching-chk" id="Teaching-chk">
                            <label class="form-check-label" for="exampleCheck1">Teaching</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="UIUXDesign-chk" id="UIUXDesign-chk">
                            <label class="form-check-label" for="exampleCheck1">UI/UX Design</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="Volunteering-chk" id="Volunteering-chk">
                            <label class="form-check-label" for="exampleCheck1">Volunteering</label>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="form-group">
                <label for="title">Number of Seats</label>
                <input type="number" class="form-control" min="1" max="10000" value="1" name="numberSeats-txt" id="numberSeats-txt" required >
            </div>
            <div class="form-group">
                <label for="internship-description">Description</label>
                <textarea maxlength="500" name="internship-description" placeholder="Maximum Length 500" class="form-control" id="internship-description" rows="4" required ></textarea>
            </div>
            <div class="form-group">
                <button type="submit" name="postInternship-btn" class="btn btn-primary btn-block">
                    Post Internship
                </button>
            </div>
            </form>
        </div>
        
    </section>
</section>