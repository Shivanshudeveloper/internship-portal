<?php
include '../src/php/dbh.php';
$postId = mysqli_real_escape_string($conn, $_POST['postId']);
$sql = "SELECT * FROM internships WHERE id = '$postId';";
$result = mysqli_query($conn, $sql);
$resultChk = mysqli_num_rows($result);
if ($resultChk < 1) {
    echo "Error Occured!";
} else {
    while ($row = mysqli_fetch_assoc($result)) {
        echo '
        <div class="w-100 container">
        <form action="./src/php/main.php" method="POST">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" value='.$row['title'].' class="form-control" name="internship-title" id="internship-title" placeholder="Enter Title For Internship" required>
            <input type="hidden" value='.$row['id'].' class="form-control" name="postId" id="postId" placeholder="Enter Title For Internship" required>
        </div>
        <div class="form-group">
            <label for="title">Stipend</label>
            <input type="number" value='.$row['stipend'].' min="1" class="form-control" name="stipend-internship" id="stipend-internship" placeholder="10000" required >
        </div>
        <div class="form-group">
            <label for="title">Start Date</label>
            <input type="date" value='.$row['start_date'].' class="form-control" name="startDate-txt" id="startDate-internship" required >
        </div>
        <div class="form-group">
            <label for="title">End Date</label>
            <input type="date" value='.$row['end_date'].' class="form-control" name="endDate-txt" id="endDate-internship" required >
        </div>
        <div class="form-group">
            <label for="title">Number of Seats</label>
            <input type="number" value='.$row['number_seats'].' class="form-control" min="1" max="10000" value="1" name="numberSeats-txt" id="numberSeats-txt" required >
        </div>
        <div class="form-group">
            <label for="internship-description">Description</label>
            <textarea maxlength="500" name="internship-description" placeholder="Maximum Length 500" class="form-control" id="internship-description" rows="4" required >'.$row['description'].'</textarea>
        </div>
        <div class="form-group">
            <button type="submit" name="updateInternshipPost-btn" class="btn btn-primary btn-block">
                <i class="fas fa-pen-alt"></i>
                Update
            </button>
        </div>
        </form>
    </div>
        ';
    }
}
?>