<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
    <?php
        include './includes/user-navigation.inc.php';
    ?>
<section class="feature-area">
		<div class="container">
			<div class="container">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Search Internships</label>
                    <input type="search" class="form-control" id="exampleInputEmail1" placeholder="Search Your Internship">
                </div>
                <button type="submit" class="btn btn-sm float-right btn-primary">
                    <i class="fas fa-search"></i>
                    Search
                </button>
            </form><br>
            </div>
            <div id="internshipsStudent">
            <?php
                    include './src/php/dbh.php';
                    $internshipId = mysqli_real_escape_string($conn, $_POST['id']);
                    $sql = "SELECT * FROM internships WHERE id = '$internshipId';";
                    $result = mysqli_query($conn, $sql);
                    $resultChk = mysqli_num_rows($result);
                    if ($resultChk < 1) {
                        echo "<h2>No Internship Found!</h2>";
                    } else {
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '
                            <div class="card mt-3 mb-2">
                                <div class="card-body">
                                    <form method="POST" action="viewInternship-details.inc.php?type=govermentdepartment&page=dashboard">
                                        <input type="hidden" name="internshipId" id="internshipId" value='.$row['id'].'>
                                        <h5 class="card-title">'.$row['title'].'</h5>
                                        <h6 class="card-title"><i style="color: green;" class="fas fa-money-bill-wave-alt"></i>  '.$row['stipend'].'</h6>
                                        For Students of Year <span class="text-dark"> '.$row['students_year'].'</span>
                                        <p class="card-text mt-3">'.$row['description'].'</p>
                                        <i class="far fa-clock"></i> '.$row['maximum_duration'].'<br>
                                        <i class="fas fa-calendar-check"></i> <span class="text-success ml-1">'.$row['start_date'].'</span> - <span class="text-danger">'.$row['end_date'].'</span><br>
                                        <i class="fas fa-chair"></i> '.$row['number_seats'].'
                                        <p>
                                        <a class="mt-2 btn btn-primary btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            View Eligibility Criteria
                                        </a>
                                        </p>
                                        <div class="collapse" id="collapseExample">
                                        <div class="card mb-2 card-body">
                                            <span class="text-dark">'.$row['internship_criterial'].'</span>
                                        </div>
                                        </div>
                                        
                                        <a href="#" onclick="markInternshipPost('.$row['id'].');" class="float-right ml-2 btn btn-sm btn-danger card-link">
                                        <i class="fas fa-heart"></i>
                                        Mark
                                        </a>
                                        <button type="button" name="apply-forInternship-btn" id="apply-forInternship-btn" class="float-right ml-2 btn btn-sm btn-primary card-link">
                                        <i class="fas fa-thumbs-up"></i>
                                        Apply
                                        </button>
                                    </form>
                                </div>
                            </div>
                        ';
                        }
                    }
            ?>
            </div>
        <!-- Internships -->
    </section>
    
    <?php
		include './includes/footer.inc.php';
    ?>
    </body>
    </html>