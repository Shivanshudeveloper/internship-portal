<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
    ?>
    <!-- Start Banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
                        <i class="fas fa-envelope-open-text"></i>
						Verify Your Email First
					</h1>
					<?php 
						$register = $_GET['register'];
						echo '
						<a class="btn btn-primary" href="phone-verification.php?register='.$register.'">
							<i class="fas fa-sign-in-alt"></i>
							Proceed With Phone Number
                    	</a>
						' 
					?>
					
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div>
	</section>
    <!-- End Banner Area -->
	<div class="container mt-4 mb-4 w-50">
    </div>
	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>