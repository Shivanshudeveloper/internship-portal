<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
    <?php
        include './includes/user-navigation.inc.php';
    ?>
<section class="feature-area">
		<div class="container">
			<div class="container">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Search Internships</label>
                    <input type="search" class="form-control" id="exampleInputEmail1" placeholder="Search Your Internship">
                </div>
                <button type="submit" class="btn btn-sm float-right btn-primary">
                    <i class="fas fa-search"></i>
                    Search
                </button>
            </form><br>
            </div>
            <br>
            <div class="container" id="internshipsStudent">
            <?php
                    include './src/php/dbh.php';
                    $userEmail = mysqli_real_escape_string($conn, $_GET['email']);
                    $sql = "SELECT * FROM internship_applied WHERE user_email = '$userEmail';";
                    $result = mysqli_query($conn, $sql);
                    $resultChk = mysqli_num_rows($result);
                    if ($resultChk < 1) {
                        echo "<h2>No Internship Found!</h2>";
                    } else {
                        while ($row = mysqli_fetch_assoc($result)) {
                            $internshipId = $row['internship_id'];
                            $sql_1 = "SELECT * FROM internships WHERE id = '$internshipId';";
                            $result_1 = mysqli_query($conn, $sql_1);
                            $resultChk_1 = mysqli_num_rows($result_1);
                            if ($resultChk_1 < 1) {
                                echo "<h2>No Internship Found!</h2>";
                            } else {
                                while ($row_1 = mysqli_fetch_assoc($result_1)) {
                                    echo '
                                    <div class="card mb-3">
                                        <div class="card-body">
                                        <form action="overview-student.inc.php?type=student&page=dashboard" method="POST">
                                            <input type="hidden" name="id" value='.$row_1['id'].'>
                                            <h5 class="card-title">'.$row_1['title'].'</h5>
                                            <span class="float-right"><i class="fas fa-download"></i> <a href="./student-internship-pdf.inc.php?id='.$row_1['id'].'" target="_blank" class="btn btn-link">Download PDF</a></span><br>
                                            <h6 class="card-title"><i style="color: green;" class="fas fa-money-bill-wave-alt"></i>  '.$row_1['stipend'].'</h6>
                                            <p class="card-text">'.$row_1['description'].'</p>
                                            <button class="btn btn-sm btn-primary">View</button>
                                            <a href="#" class="btn btn-danger btn-sm ml-1" onclick="cancel_StudentInternship('.$row_1['id'].')">
                                            <i class="far fa-times-circle"></i>
                                            Request for Cancellation
                                            </a>
                                        </form>
                                        </div>
                                    </div>
                                ';
                                }
                            }
                        }
                    }
            ?>
            </div>
        <!-- Internships -->
    </section>
    
    <?php
		include './includes/footer.inc.php';
    ?>
    </body>
    </html>