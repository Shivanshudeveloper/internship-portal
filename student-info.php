<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
	?>
	<!-- Start Banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
                        <i class="fas fa-graduation-cap"></i>
						Your Fields of Interest
					</h1>
					<div class="link-nav">
						<span class="box">
							<a href="index.php">Home </a>
							<i class="lnr lnr-arrow-right"></i>
							<a href="register.php"><?php echo $_GET['register']; ?></a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div>
	</section>
  <!-- End Banner Area -->
<div id="messages"></div>

<div class="container mt-2 mb-2 w-50">
<h3 class="mb-2">Student</h3>
<form action="./src/php/main.php" method="POST">
        <div class="form-group">
            <div class="form-group">
                <label for="exampleInputEmail1">Skype ID</label>
                <input type="text" class="form-control" name="skypeId"  placeholder="Your Skype ID">
                <small id="emailHelp" class="form-text text-muted">Opstional</small>
            </div>
        </div>
        <div class="form-group">
            <?php echo '<input type="hidden" name="studentEmail" value='.$_GET['email'].'>' ?>
            <label for="exampleInputEmail1">Select Program</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Programs</label>
              </div>
              <select class="custom-select" name="StuSelectProgram">
                <option selected>Choose...</option>
                <option value="Diploma">Diploma</option>
                <option value="Undergraduate">Undergraduate</option>
                <option value="Postgraduate">Postgraduate</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Your Field For Internships</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">1st Preference</label>
              </div>
              <select class="custom-select" name="internshipFields-firstPreference-select">
                <option selected>Choose...</option>
                <option value="Architecture">Architecture</option>
                <option value="Interior Design">Interior Design</option>
                <option value="Commerce">Commerce</option>
                <option value="Accounts">Accounts</option>
                <option value="Chartered Accountancy">Chartered Accountancy</option>
                <option value="Design">Design</option>
                <option value="Animation">Animation</option>
                <option value="Fashion Design">Fashion Design</option>
                <option value="Graphic Design">Graphic Design</option>
                <option value="Merchandise Design">Merchandise Design</option>
                <option value="Engineering">Engineering</option>
                <option value="Aerospace Engineering">Aerospace Engineering</option>
                <option value="Biotechnology Engineering">Biotechnology Engineering</option>
                <option value="Chemical Engineering">Chemical Engineering</option>
                <option value="Civil Engineering">Civil Engineering</option>
                <option value="Computer Vision">Computer Vision</option>
                <option value="Electrical Engineering">Electrical Engineering</option>
                <option value="Electronics Engineering">Electronics Engineering</option>
                <option value="Energy Science &amp; Engineering">Energy Science &amp; Engineering</option>
                <option value="Engineering Design">Engineering Design</option>
                <option value="Engineering Physics">Engineering Physics</option>
                <option value="Game Development">Game Development</option>
                <option value="Material Science">Material Science</option>
                <option value="Mechanical Engineering">Mechanical Engineering</option>
                <option value="Metallurgical Engineering">Metallurgical Engineering</option>
                <option value="Mobile App Development">Mobile App Development</option>
                <option value="Naval Architecture and Ocean Engineeering">Naval Architecture and Ocean Engineeering</option>
                <option value="Network Engineering">Network Engineering</option>
                <option value="Petroleum Engineering">Petroleum Engineering</option>
                <option value="Programming">Programming</option>
                <option value="Software Development">Software Development</option>
                <option value="Software Testing">Software Testing</option>
                <option value="Web Development">Web Development</option>
                <option value="Hospitality">Hospitality</option>
                <option value="Hotel Management">Hotel Management</option>
                <option value="Travel &amp; Tourism">Travel &amp; Tourism</option>
                <option value="MBA">MBA</option>
                <option value="Data Entry">Data Entry</option>
                <option value="Hospitality">Hospitality</option>
                <option value="Digital Marketing">Digital Marketing</option>
                <option value="Finance">Finance</option>
                <option value="General Management">General Management</option>
                <option value="Human Resources (HR)">Human Resources (HR)</option>
                <option value="Market/Business Research">Market/Business Research</option>
                <option value="Marketing">Marketing</option>
                <option value="Operations">Operations</option>
                <option value="Sales">Sales</option>
                <option value="Media">Media</option>
                <option value="Cinematography">Cinematography</option>
                <option value="Content Writing">Content Writing</option>
                <option value="Film Making">Film Making</option>
                <option value="Journalism">Journalism</option>
                <option value="Motion Graphics">Motion Graphics</option>
                <option value="Photography">Photography</option>
                <option value="Public Relations (PR)">Public Relations (PR)</option>
                <option value="Social Media Marketing">Social Media Marketing</option>
                <option value="Video Making/Editing">Video Making/Editing</option>
                <option value="Videography">Videography</option>
                <option value="Science">Science</option>
                <option value="Biology">Biology</option>
                <option value="Chemistry">Chemistry</option>
                <option value="Mathematics">Mathematics</option>
                <option value="Physics">Physics</option>
                <option value="Statistics">Statistics</option>
                <option value="Agriculture &amp; Food Engineering">Agriculture &amp; Food Engineering</option>
                <option value="Campus Ambassador">Campus Ambassador</option>
                <option value="Company Secretary (CS)">Company Secretary (CS)</option>
                <option value="Data Science">Data Science</option>
                <option value="Event Management">Event Management</option>
                <option value="Humanities">Humanities</option>
                <option value="Law">Law</option>
                <option value="Medicine">Medicine</option>
                <option value="Pharmaceutical">Pharmaceutical</option>
                <option value="Teaching">Teaching</option>
                <option value="UI/UX Design">UI/UX Design</option>
                <option value="Volunteering">Volunteering</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Your Field For Internships</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">2nd Preference</label>
              </div>
              <select class="custom-select" name="internshipFields-secondPreference-select">
                <option selected>Choose...</option>
                <option value="Architecture">Architecture</option>
                <option value="Interior Design">Interior Design</option>
                <option value="Commerce">Commerce</option>
                <option value="Accounts">Accounts</option>
                <option value="Chartered Accountancy">Chartered Accountancy</option>
                <option value="Design">Design</option>
                <option value="Animation">Animation</option>
                <option value="Fashion Design">Fashion Design</option>
                <option value="Graphic Design">Graphic Design</option>
                <option value="Merchandise Design">Merchandise Design</option>
                <option value="Engineering">Engineering</option>
                <option value="Aerospace Engineering">Aerospace Engineering</option>
                <option value="Biotechnology Engineering">Biotechnology Engineering</option>
                <option value="Chemical Engineering">Chemical Engineering</option>
                <option value="Civil Engineering">Civil Engineering</option>
                <option value="Computer Vision">Computer Vision</option>
                <option value="Electrical Engineering">Electrical Engineering</option>
                <option value="Electronics Engineering">Electronics Engineering</option>
                <option value="Energy Science &amp; Engineering">Energy Science &amp; Engineering</option>
                <option value="Engineering Design">Engineering Design</option>
                <option value="Engineering Physics">Engineering Physics</option>
                <option value="Game Development">Game Development</option>
                <option value="Material Science">Material Science</option>
                <option value="Mechanical Engineering">Mechanical Engineering</option>
                <option value="Metallurgical Engineering">Metallurgical Engineering</option>
                <option value="Mobile App Development">Mobile App Development</option>
                <option value="Naval Architecture and Ocean Engineeering">Naval Architecture and Ocean Engineeering</option>
                <option value="Network Engineering">Network Engineering</option>
                <option value="Petroleum Engineering">Petroleum Engineering</option>
                <option value="Programming">Programming</option>
                <option value="Software Development">Software Development</option>
                <option value="Software Testing">Software Testing</option>
                <option value="Web Development">Web Development</option>
                <option value="Hospitality">Hospitality</option>
                <option value="Hotel Management">Hotel Management</option>
                <option value="Travel &amp; Tourism">Travel &amp; Tourism</option>
                <option value="MBA">MBA</option>
                <option value="Data Entry">Data Entry</option>
                <option value="Hospitality">Hospitality</option>
                <option value="Digital Marketing">Digital Marketing</option>
                <option value="Finance">Finance</option>
                <option value="General Management">General Management</option>
                <option value="Human Resources (HR)">Human Resources (HR)</option>
                <option value="Market/Business Research">Market/Business Research</option>
                <option value="Marketing">Marketing</option>
                <option value="Operations">Operations</option>
                <option value="Sales">Sales</option>
                <option value="Media">Media</option>
                <option value="Cinematography">Cinematography</option>
                <option value="Content Writing">Content Writing</option>
                <option value="Film Making">Film Making</option>
                <option value="Journalism">Journalism</option>
                <option value="Motion Graphics">Motion Graphics</option>
                <option value="Photography">Photography</option>
                <option value="Public Relations (PR)">Public Relations (PR)</option>
                <option value="Social Media Marketing">Social Media Marketing</option>
                <option value="Video Making/Editing">Video Making/Editing</option>
                <option value="Videography">Videography</option>
                <option value="Science">Science</option>
                <option value="Biology">Biology</option>
                <option value="Chemistry">Chemistry</option>
                <option value="Mathematics">Mathematics</option>
                <option value="Physics">Physics</option>
                <option value="Statistics">Statistics</option>
                <option value="Agriculture &amp; Food Engineering">Agriculture &amp; Food Engineering</option>
                <option value="Campus Ambassador">Campus Ambassador</option>
                <option value="Company Secretary (CS)">Company Secretary (CS)</option>
                <option value="Data Science">Data Science</option>
                <option value="Event Management">Event Management</option>
                <option value="Humanities">Humanities</option>
                <option value="Law">Law</option>
                <option value="Medicine">Medicine</option>
                <option value="Pharmaceutical">Pharmaceutical</option>
                <option value="Teaching">Teaching</option>
                <option value="UI/UX Design">UI/UX Design</option>
                <option value="Volunteering">Volunteering</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Your Field For Internships</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">3rd Preference</label>
              </div>
              <select class="custom-select" name="internshipFields-thirdPreference-select">
                <option selected>Choose...</option>
                <option value="Architecture">Architecture</option>
                <option value="Interior Design">Interior Design</option>
                <option value="Commerce">Commerce</option>
                <option value="Accounts">Accounts</option>
                <option value="Chartered Accountancy">Chartered Accountancy</option>
                <option value="Design">Design</option>
                <option value="Animation">Animation</option>
                <option value="Fashion Design">Fashion Design</option>
                <option value="Graphic Design">Graphic Design</option>
                <option value="Merchandise Design">Merchandise Design</option>
                <option value="Engineering">Engineering</option>
                <option value="Aerospace Engineering">Aerospace Engineering</option>
                <option value="Biotechnology Engineering">Biotechnology Engineering</option>
                <option value="Chemical Engineering">Chemical Engineering</option>
                <option value="Civil Engineering">Civil Engineering</option>
                <option value="Computer Vision">Computer Vision</option>
                <option value="Electrical Engineering">Electrical Engineering</option>
                <option value="Electronics Engineering">Electronics Engineering</option>
                <option value="Energy Science &amp; Engineering">Energy Science &amp; Engineering</option>
                <option value="Engineering Design">Engineering Design</option>
                <option value="Engineering Physics">Engineering Physics</option>
                <option value="Game Development">Game Development</option>
                <option value="Material Science">Material Science</option>
                <option value="Mechanical Engineering">Mechanical Engineering</option>
                <option value="Metallurgical Engineering">Metallurgical Engineering</option>
                <option value="Mobile App Development">Mobile App Development</option>
                <option value="Naval Architecture and Ocean Engineeering">Naval Architecture and Ocean Engineeering</option>
                <option value="Network Engineering">Network Engineering</option>
                <option value="Petroleum Engineering">Petroleum Engineering</option>
                <option value="Programming">Programming</option>
                <option value="Software Development">Software Development</option>
                <option value="Software Testing">Software Testing</option>
                <option value="Web Development">Web Development</option>
                <option value="Hospitality">Hospitality</option>
                <option value="Hotel Management">Hotel Management</option>
                <option value="Travel &amp; Tourism">Travel &amp; Tourism</option>
                <option value="MBA">MBA</option>
                <option value="Data Entry">Data Entry</option>
                <option value="Hospitality">Hospitality</option>
                <option value="Digital Marketing">Digital Marketing</option>
                <option value="Finance">Finance</option>
                <option value="General Management">General Management</option>
                <option value="Human Resources (HR)">Human Resources (HR)</option>
                <option value="Market/Business Research">Market/Business Research</option>
                <option value="Marketing">Marketing</option>
                <option value="Operations">Operations</option>
                <option value="Sales">Sales</option>
                <option value="Media">Media</option>
                <option value="Cinematography">Cinematography</option>
                <option value="Content Writing">Content Writing</option>
                <option value="Film Making">Film Making</option>
                <option value="Journalism">Journalism</option>
                <option value="Motion Graphics">Motion Graphics</option>
                <option value="Photography">Photography</option>
                <option value="Public Relations (PR)">Public Relations (PR)</option>
                <option value="Social Media Marketing">Social Media Marketing</option>
                <option value="Video Making/Editing">Video Making/Editing</option>
                <option value="Videography">Videography</option>
                <option value="Science">Science</option>
                <option value="Biology">Biology</option>
                <option value="Chemistry">Chemistry</option>
                <option value="Mathematics">Mathematics</option>
                <option value="Physics">Physics</option>
                <option value="Statistics">Statistics</option>
                <option value="Agriculture &amp; Food Engineering">Agriculture &amp; Food Engineering</option>
                <option value="Campus Ambassador">Campus Ambassador</option>
                <option value="Company Secretary (CS)">Company Secretary (CS)</option>
                <option value="Data Science">Data Science</option>
                <option value="Event Management">Event Management</option>
                <option value="Humanities">Humanities</option>
                <option value="Law">Law</option>
                <option value="Medicine">Medicine</option>
                <option value="Pharmaceutical">Pharmaceutical</option>
                <option value="Teaching">Teaching</option>
                <option value="UI/UX Design">UI/UX Design</option>
                <option value="Volunteering">Volunteering</option>
              </select>
            </div>
          </div>
          <div class="form-group">
          <label for="exampleInputEmail1">What type of internships are you looking for?</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="internship-Looking-select">Looking for</label>
              </div>
              <select class="custom-select" name="Stutype-select">
                <option selected>Choose...</option>
                <option value="Full time internship only">Full time internship only</option>
                <option value="Work from home Part time">Work from home (Part time)</option>
                <option value="Both">Both</option>
              </select>
            </div>
          </div>
          <div class="form-group">
          <label for="exampleInputEmail1">In which state you like to work?</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">State</label>
              </div>
              <select class="custom-select" name="StuSelectState">
                <option selected>Select State</option>
                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                <option value="Andhra Pradesh">Andhra Pradesh</option>
                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                <option value="Assam">Assam</option>
                <option value="Bihar">Bihar</option>
                <option value="Chandigarh">Chandigarh</option>
                <option value="Chhattisgarh">Chhattisgarh</option>
                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                <option value="Daman and Diu">Daman and Diu</option>
                <option value="Delhi">Delhi</option>
                <option value="Goa">Goa</option>
                <option value="Gujarat">Gujarat</option>
                <option value="Haryana">Haryana</option>
                <option value="Himachal Pradesh">Himachal Pradesh</option>
                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                <option value="Jharkhand">Jharkhand</option>
                <option value="Karnataka">Karnataka</option>
                <option value="Kerala">Kerala</option>
                <option value="Lakshadweep">Lakshadweep</option>
                <option value="Madhya Pradesh">Madhya Pradesh</option>
                <option value="Maharashtra">Maharashtra</option>
                <option value="Manipur">Manipur</option>
                <option value="Meghalaya">Meghalaya</option>
                <option value="Mizoram">Mizoram</option>
                <option value="Nagaland">Nagaland</option>
                <option value="Orissa">Orissa</option>
                <option value="Pondicherry">Pondicherry</option>
                <option value="Punjab">Punjab</option>
                <option value="Rajasthan">Rajasthan</option>
                <option value="Sikkim">Sikkim</option>
                <option value="Tamil Nadu">Tamil Nadu</option>
                <option value="Tripura">Tripura</option>
                <option value="Uttaranchal">Uttaranchal</option>
                <option value="Uttar Pradesh">Uttar Pradesh</option>
                <option value="West Bengal">West Bengal</option>
              </select>
            </div>
          </div>
          <button type="submit" name="submit-student-interest-btn" class="btn btn-block btn-primary">
            Continue
            <i class="fas fa-long-arrow-alt-right"></i>
          </button>
</form>
</div>
<?php
	include './includes/footer.inc.php';
?>
  
</body>

</html>