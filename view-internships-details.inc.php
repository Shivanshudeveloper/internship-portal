
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
        include './includes/user-navigation.inc.php';
    ?>
    <section class="feature-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="section-title text-center">
						<h1>
                            <i class="fas fa-chart-line"></i>
							 Analytics of Internships
                        </h1>
					</div>
				</div>
			</div>
			<div class="container">
                <?php
                    include './src/php/dbh.php';
                    $internshipId = $_GET['internshipId'];
                    $internshipStipend = '';
                    $internshipStartDate = '';
                    $internshipEndDate = '';
                    $sql = "SELECT * FROM internships WHERE id = '$internshipId';";
                    $result = mysqli_query($conn, $sql);
                    $resultChk = mysqli_num_rows($result);
                    if ($resultChk < 1) {
                        echo "No Internship Found!";
                    } else {
                        echo '
                            <form action="./src/php/main.php" method="POST">
                                <input type="hidden" value='.$internshipId.'/>
                                <button type="submit" class="btn float-right btn-sm btn-success">
                                    <i class="fas fa-file-excel"></i>
                                    Download Excel
                                </button>
                            </form><br>
                        ';
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo '<h3>'.$row['title'].'</h3>
                            <p class="mt-1">'.$row['description'].'</p>
                            
                            ';
                            $internshipStipend = $row['stipend'];
                            $internshipStartDate = $row['start_date'];
                            $internshipEndDate = $row['end_date'];
                        }
                    }
                ?>
                <hr>
                <h3 class="mt-2">Students Applied</h3>
                <?php
                $sql = "SELECT * FROM internship_applied WHERE internship_id = '$internshipId';";
                $result = mysqli_query($conn, $sql);
                $resultChk = mysqli_num_rows($result);
                echo '
                    <h4 class="mt-2">
                    <i class="fas fa-user-graduate"></i>
                    Total Number of Students Applied = '.$resultChk.'</h4>
                    <h4 class="mt-2 text-success">
                    <i class="fas fa-calendar-week"></i>
                    Internship starting from = '.$internshipStartDate.'</h4>
                    <h4 class="mt-2 text-danger">
                    <i class="fas fa-calendar-week"></i>
                    Internship ending on = '.$internshipEndDate.'</h4>
                    <h4 class="mt-2 text-success">
                    <i class="fas fa-rupee-sign"></i>
                    Stipend for the internship = '.$internshipStipend.'</h4>
                ';
                ?>
            </div>
		</div>
	</section>

	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>
    <!-- Body Comes Here -->
	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>