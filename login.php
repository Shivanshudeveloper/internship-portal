<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
    ?>
    <!-- Start Banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Let's Roll In
					</h1>
					<div class="link-nav">
						<span class="box">
							<a href="index.php">Home </a>
							<i class="lnr lnr-arrow-right"></i>
							<a href="login.php">login</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div>
	</section>
    <!-- End Banner Area -->
	<div class="container mt-4 mb-4 w-50">
    <form >
    <h3 class="mb-30">Login</h3>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email-text" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="password-pwd" placeholder="Password">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Remember Me</label>
        </div>
        <button type="button" id="login-btn" class="btn btn-block btn-primary">
            <i class="fas fa-sign-in-alt"></i>
            Login
        </button>
    </form>
    </div>


	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>