<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
	?>
	<!-- Start Banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
                        <i class="fas fa-briefcase"></i>
						A Bit Information Requried
					</h1>
					<div class="link-nav">
						<span class="box">
							<a href="index.php">Home </a>
							<i class="lnr lnr-arrow-right"></i>
							<a href="register.php"><?php echo $_GET['register']; ?></a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div>
	</section>
  <!-- End Banner Area -->
<div id="messages"></div>

<div class="container mt-3 mb-2 w-50">
<h3 class="mb-2">Goverment Department</h3>
<form action="./src/php/main.php" method="POST">
        <div class="form-group">
            <div class="form-group">
                <label for="exampleInputEmail1">Skype ID</label>
                <input type="text" class="form-control" name="skypeId"  placeholder="Your Skype ID">
                <small id="emailHelp" class="form-text text-muted">Opstional</small>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group">
                <label for="exampleInputEmail1">Address</label>
                <input type="text" class="form-control" name="address"  placeholder="Department Address">
                <small id="emailHelp" class="form-text text-muted">Opstional</small>
            </div>
        </div>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupFileAddon01">Department Logo</span>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="fileButton"
                aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
        </div>
        <div class="form-group mt-2">
        <progress value="0" max="100" style="width: 100%;" id="uploader">0%</progress>
        </div>
        <div class="form-group">
            <?php echo '<input type="hidden" id="govDepartmentEmail" name="govDepartmentEmail" value='.$_GET['email'].'>' ?>
        </div>    
        <button type="submit" id="submit-govermentDepartment-info-btn"  name="submit-govermentDepartment-info-btn" class="btn btn-block btn-primary">
            Continue
        </button>
</form>
</div>
<?php
	include './includes/footer.inc.php';
?>
  
</body>

</html>