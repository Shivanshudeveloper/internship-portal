<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
        include './includes/user-navigation.inc.php';
    ?>
    <div id="root"></div>

	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>