<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
	?>
	<!-- Start Banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Choose your Option to Register
					</h1>
					<p>All India Council for Technical Education gives you the best internship on your choice</p>
					<div class="link-nav">
						<span class="box">
							<a href="index.php">Home </a>
							<i class="lnr lnr-arrow-right"></i>
							<a href="register.php">Register</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div>
	</section>
    <!-- End Banner Area -->
    
    <!--Start Feature Area -->
	<section class="feature-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="section-title text-center">
						<h1>Choose Where to Apply</h1>
					</div>
				</div>
			</div>
			<div class="feature-inner row">
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
						<i class="ti-crown"></i>
						<h2>Corporate Sector</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
                            <a href="register.inc.php?register=Corporate Sector">Register Now</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
						<i class="ti-briefcase"></i>
						<h2>Govermental</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                            <a href="register.inc.php?register=Govermental Department">Register Now</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
						<i class="ti-medall-alt"></i>
						<h2>Students</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
							<a href="register.inc.php?register=For Student Registration">Register Now</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
                        <i class="fas fa-chalkboard-teacher"></i>
						<h2>Faculty</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
                            <a href="register.inc.php?register=Faculty">Register Now</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
						<i class="ti-headphone-alt"></i>
						<h2>Register Institute</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                            <a href="register.inc.php?register=Institute">Register Now</a>
						</div>
					</div>
                </div>
                <div class="col-lg-4 col-md-6">
					<div class="feature-item">
                        <i class="fab fa-old-republic"></i>
						<h2>Public Sector</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                            <a href="register.inc.php?register=Public Sector">Register Now</a>
						</div>
					</div>
                </div>
                <div class="col-lg-4 col-md-6">
					<div class="feature-item">
                        <i class="fas fa-globe-europe"></i>
						<h2>Aggregator</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                            <a href="register.inc.php?register=Aggregator">Register Now</a>
						</div>
					</div>
                </div>
                <div class="col-lg-4 col-md-6">
					<div class="feature-item">
                        <i class="fab fa-galactic-republic"></i>
						<h2>Govermental</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                            <a href="register.inc.php?register=Govermental">Register Now</a>
						</div>
					</div>
                </div>
                <div class="col-lg-4 col-md-6">
					<div class="feature-item">
                        <i class="fas fa-mask"></i>
						<h2>Skill Council</h2>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                            <a href="register.inc.php?register=Skill Council">Register Now</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- End Feature Area -->


	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>