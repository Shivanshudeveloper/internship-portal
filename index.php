<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
	?>
	<section class="home-banner-area relative">
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-center">
				<div class="banner-content col-md-12">
					<img src="./img/aicte/logo.jpg" alt="AICTE Logo" width="20%" style="margin-top: 80px;" srcset="">
					<h1 class="wow fadeIn" data-wow-duration="4s">All India Council for Technical Education</h1>
					<p class="text-white">
					Go Ahead.Get an Internship
					</p>

					<div class="input-wrap">
						<form action="" class="form-box d-flex justify-content-between">
							<input type="text" placeholder="Search Internship" class="form-control" name="username">
							<button type="submit" class="btn search-btn">Search</button>
						</form>
					</div>
					<h4 class="text-white" style="margin-bottom: 20px;">Top Internship</h4>
						<a href="#" data-wow-duration="1s" data-wow-delay=".3s" class="primary-btn transparent mr-10 mb-10 wow fadeInDown">Ruby
							on Rails</a>
						<a href="#" data-wow-duration="1s" data-wow-delay=".6s" class="primary-btn transparent mr-10 mb-10 wow fadeInDown">Python</a>
						<a href="#" data-wow-duration="1s" data-wow-delay=".9s" class="primary-btn transparent mr-10 mb-10 wow fadeInDown">Marketing</a>
						<a href="#" data-wow-duration="1s" data-wow-delay="1.2s" class="primary-btn transparent mr-10 mb-10 wow fadeInDown">UI/UX
							Design
						</a>
				</div>
			</div>
		</div>
		<!-- <div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div> -->
	</section>
	<!-- End Banner Area -->


	<!-- Start About Area -->
	<!-- <section class="about-area section-gap">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-lg-5 col-md-6 about-left">
					<img class="img-fluid" src="img/about.jpg" alt="">
				</div>
				<div class="offset-lg-1 col-lg-6 offset-md-0 col-md-12 about-right">
					<h1>
						Over 2500 Courses <br> from 5 Platform
					</h1>
					<div class="wow fadeIn" data-wow-duration="1s">
						<p>
							There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s exciting to think
							about setting up your own viewing station. In the life of any aspiring astronomer that it is time to buy that first
							telescope. It’s exciting to think about setting up your own viewing station.
						</p>
					</div>
					<a href="courses.html" class="primary-btn">Explore Courses</a>
				</div>
			</div>
		</div>
	</section> -->
	<!-- End About Area -->
	<!-- Start Courses Area -->
	<section class="courses-area" style="margin-top: 30px;">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5 about-right">
					<h1>
						Internships in popular categories
					</h1>
					<div class="wow fadeIn" data-wow-duration="1s">
						<p>
							Its Hot! Pick an Internship
							Internship add enhances your market value as a fresher.
							Get your resume heavy 
						</p>
					</div>
					<a href="courses.html" class="primary-btn white">Explore Courses</a>
				</div>
				<div class="offset-lg-1 col-lg-6">
					<div class="courses-right">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12">
								<ul class="courses-list">
									<li>
										<a class="wow fadeInLeft" href="#" data-wow-duration="1s" data-wow-delay=".1s">
											<i class="fa fa-book"></i> Development
										</a>
									</li>
									<li>
										<a class="wow fadeInLeft" href="#" data-wow-duration="1s" data-wow-delay=".3s">
											<i class="fa fa-book"></i> IT & Software
										</a>
									</li>
									<li>
										<a class="wow fadeInLeft" href="#" data-wow-duration="1s" data-wow-delay=".5s">
											<i class="fa fa-book"></i> Photography
										</a>
									</li>
									<li>
										<a class="wow fadeInLeft" href="#" data-wow-duration="1s" data-wow-delay=".7s">
											<i class="fa fa-book"></i> Language
										</a>
									</li>
									<li>
										<a class="wow fadeInLeft" href="#" data-wow-duration="1s" data-wow-delay=".9s">
											<i class="fa fa-book"></i> Life Science
										</a>
									</li>
									<li>
										<a class="wow fadeInLeft" href="#" data-wow-duration="1s" data-wow-delay="1.1s">
											<i class="fa fa-book"></i> Business
										</a>
									</li>
									<li>
										<a class="wow fadeInLeft" href="#" data-wow-duration="1s" data-wow-delay="1.3s">
											<i class="fa fa-book"></i> Socoal Science
										</a>
									</li>
								</ul>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12">
								<ul class="courses-list">
									<li>
										<a class="wow fadeInRight" href="#" data-wow-duration="1s" data-wow-delay="1.3s">
											<i class="fa fa-book"></i> Data Science
										</a>
									</li>
									<li>
										<a class="wow fadeInRight" href="#" data-wow-duration="1s" data-wow-delay="1.1s">
											<i class="fa fa-book"></i> Design
										</a>
									</li>
									<li>
										<a class="wow fadeInRight" href="#" data-wow-duration="1s" data-wow-delay=".9s">
											<i class="fa fa-book"></i> Training
										</a>
									</li>
									<li>
										<a class="wow fadeInRight" href="#" data-wow-duration="1s" data-wow-delay=".7s">
											<i class="fa fa-book"></i> Humanities
										</a>
									</li>
									<li>
										<a class="wow fadeInRight" href="#" data-wow-duration="1s" data-wow-delay=".5s">
											<i class="fa fa-book"></i> Marketing
										</a>
									</li>
									<li>
										<a class="wow fadeInRight" href="#" data-wow-duration="1s" data-wow-delay=".3s">
											<i class="fa fa-book"></i> Economics
										</a>
									</li>
									<li>
										<a class="wow fadeInRight" href="#" data-wow-duration="1s" data-wow-delay=".1s">
											<i class="fa fa-book"></i> Personal Dev
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Courses Area -->

	<!--Start Feature Area -->
	<section class="feature-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="section-title text-center">
						<h1>Internships in popular cities</h1>
						<p>
							Search Your Internship as per your needs in the most popular cities in India.
						</p>
					</div>
				</div>
			</div>
			<div class="feature-inner row">
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="40" height="40"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#f1c40f"><g id="surface1"><path d="M82.56,10.32c-1.78719,0 -3.27875,1.37063 -3.42656,3.15781l-3.18469,38.12219h-7.14875c-1.58562,0 -2.95625,1.075 -3.3325,2.60688l-3.01,11.99969c-4.64937,1.66625 -8.26406,5.68406 -9.09719,10.66937l-3.42656,22.2525c-0.55094,3.01 -1.70656,5.50938 -3.34594,7.51156h-1.86781c-1.89469,0 -3.44,1.54531 -3.44,3.44v4.13875c-3.84312,1.54531 -6.88,5.49594 -6.88,9.62125h-10.32c-1.89469,0 -3.44,1.54531 -3.44,3.44c0,4.27313 0.79281,8.02219 2.62031,11.0725c-5.49594,1.80063 -9.50031,6.92031 -9.50031,13.0075c-1.89469,0 -3.44,1.54531 -3.44,3.44v3.44c0,1.89469 1.54531,3.44 3.44,3.44h144.48c1.89469,0 3.44,-1.54531 3.44,-3.44v-3.44c0,-1.89469 -1.54531,-3.44 -3.44,-3.44c0,-6.08719 -4.00437,-11.20687 -9.50031,-13.0075c1.8275,-3.05031 2.62031,-6.79937 2.62031,-11.0725c0,-1.89469 -1.54531,-3.44 -3.44,-3.44h-10.32c0,-4.12531 -3.03687,-8.07594 -6.88,-9.62125v-4.13875c0,-1.89469 -1.54531,-3.44 -3.44,-3.44h-1.86781c-1.63937,-2.00219 -2.795,-4.48812 -3.3325,-7.40406l-3.45344,-22.40031c-0.81969,-4.945 -4.43437,-8.96281 -9.08375,-10.62906l-3.01,-11.99969c-0.37625,-1.53188 -1.74687,-2.60688 -3.3325,-2.60688h-7.14875l-3.18469,-38.12219c-0.14781,-1.78719 -1.63938,-3.15781 -3.42656,-3.15781zM71.4875,58.48h29.025l1.72,6.88h-32.465zM65.36,79.12l3.44,3.44l-3.44,3.44l-3.44,-3.44zM79.12,79.12l3.44,3.44l-3.44,3.44l-3.44,-3.44zM92.88,79.12l3.44,3.44l-3.44,3.44l-3.44,-3.44zM106.64,79.12l3.44,3.44l-3.44,3.44l-3.44,-3.44zM58.48,86l3.44,3.44l-3.44,3.44l-3.44,-3.44zM72.24,86l3.44,3.44l-3.44,3.44l-3.44,-3.44zM86,86l3.44,3.44l-3.44,3.44l-3.44,-3.44zM99.76,86l3.44,3.44l-3.44,3.44l-3.44,-3.44zM113.52,86l3.44,3.44l-3.44,3.44l-3.44,-3.44zM65.36,92.88l3.44,3.44l-3.44,3.44l-3.44,-3.44zM79.12,92.88l3.44,3.44l-3.44,3.44l-3.44,-3.44zM92.88,92.88l3.44,3.44l-3.44,3.44l-3.44,-3.44zM106.64,92.88l3.44,3.44l-3.44,3.44l-3.44,-3.44zM58.48,99.76l3.44,3.44l-3.44,3.44l-3.44,-3.44zM72.24,99.76l3.44,3.44l-3.44,3.44l-3.44,-3.44zM86,99.76l3.44,3.44l-3.44,3.44l-3.44,-3.44zM99.76,99.76l3.44,3.44l-3.44,3.44l-3.44,-3.44zM113.52,99.76l3.44,3.44l-3.44,3.44l-3.44,-3.44zM28.33969,130.72h115.32063c-0.34938,1.65281 -0.47031,3.7625 -1.10188,4.63594c-1.10187,1.53188 -2.37844,2.24406 -4.95844,2.24406h-103.2c-2.58,0 -3.85656,-0.71219 -4.95844,-2.24406c-0.63156,-0.87344 -0.7525,-2.98312 -1.10188,-4.63594z"></path></g></g></g></svg>
						<h4>Chennai</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
							<p>
								Chennai, on the Bay of Bengal in eastern India, is the capital of the state of Tamil Nadu.
							</p>
						</div>
						<a href="#">View</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="45" height="45"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e67e22"><path d="M85.31469,29.58266l-2.75469,3.44c-1.032,1.376 -2.40935,2.752 -3.09735,3.44c-15.48,2.752 -20.29331,9.97869 -21.32531,16.51469c-3.784,0.688 -6.88,3.09197 -8.6,6.18797c6.536,3.44 11.69734,8.25869 11.69734,13.07469h3.44c0,-9.976 15.824,-18.92 18.92,-20.64l1.72,-1.03469l1.72,1.03469c3.096,1.72 18.92,10.664 18.92,20.64v0.34265h3.44v-0.34265c0.344,-4.472 6.192,-10.32 12.04,-13.76c-0.344,-1.032 -1.032,-1.71731 -1.72,-2.40531c-1.72,-2.064 -4.47737,-3.44537 -7.22937,-3.78937c-1.032,-6.536 -6.18931,-13.75731 -21.32531,-16.16531c-0.688,-0.344 -2.06535,-2.06534 -3.09735,-3.09734zM85.65735,58.13735c-6.192,3.784 -13.76,9.97465 -13.76,14.10265v6.88h-17.2v-6.88c0,-1.032 -1.03066,-2.75065 -7.22266,-7.22265c-2.064,-1.376 -4.12934,-2.41069 -4.81734,-2.75469l-1.37735,-0.68531l-1.37735,0.68531c-1.72,0.688 -15.82265,7.56935 -15.82265,16.85735v8.6c-1.032,-0.688 -2.40666,-1.03335 -3.78266,-1.37735c-1.032,-0.344 -1.72269,-0.344 -2.75469,0c-6.536,1.72 -10.66266,6.88135 -10.66266,13.41735v44.72h158.24v-45.06265c0,-6.88 -4.12665,-12.04135 -10.66265,-13.41735c-1.032,-0.344 -1.72269,-0.344 -2.75469,0c-1.376,0.344 -2.40665,1.03335 -3.78265,1.37735v-8.6c0,-8.944 -13.07065,-15.48135 -15.82265,-16.85735l-1.37735,-0.68531l-2.40531,1.02797c-1.032,0.344 -2.41069,1.38003 -4.47469,2.41203c-3.44,2.408 -6.87865,5.84666 -7.22265,7.22266v6.88h-17.2v-6.88c0,-4.128 -7.568,-10.32 -13.76,-13.76zM65.01735,92.88h6.88v17.2h-6.88zM78.77735,92.88h13.76v17.2h-13.76zM99.41735,92.88h6.88v17.2h-6.88zM27.52,116.96h6.88v5.16l-6.88,5.50265zM52.63469,116.96h63.29063l28.21203,20.64h-117.30266zM137.6,116.96h6.88v12.72531l-6.88,-4.81063z"></path></g></g></svg>
						<h4>Bengaluru</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
							<p>
								Bengaluru (also called Bangalore) is the capital of India's southern Karnataka state.
							</p>
						</div>
						<a href="#">View</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="40" height="40"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e74c3c"><path d="M113.52,17.2c-4.128,2.9584 -6.88,4.4376 -6.88,10.32h3.44v22.704c-1.1352,-1.0664 -2.408,-1.9264 -3.44,-2.58v-6.364h-6.88v6.364c-1.6168,0.9976 -3.7152,2.5456 -5.16,4.5408c-1.4448,-1.9952 -3.5432,-3.5432 -5.16,-4.5408v-6.364h-6.88v6.364c-1.6168,0.9976 -3.7152,2.5456 -5.16,4.5408c-1.4448,-1.9952 -3.5432,-3.5432 -5.16,-4.5408v-6.364h-6.88v6.364c-1.032,0.6536 -2.3048,1.5136 -3.44,2.58v-22.704h3.44c0,-5.8824 -2.752,-7.3616 -6.88,-10.32c-4.128,2.9584 -6.88,4.4376 -6.88,10.32h3.44v65.36h61.92v-65.36h3.44c0,-5.8824 -2.752,-7.3616 -6.88,-10.32zM99.76,57.0352c0,-0.688 1.4792,-2.2016 3.44,-3.4056c1.9608,1.204 3.44,2.7176 3.44,3.4056v1.4448h-6.88zM82.56,57.0352c0,-0.688 1.4792,-2.2016 3.44,-3.4056c1.9608,1.204 3.44,2.7176 3.44,3.4056v1.4448h-6.88zM65.36,57.0352c0,-0.688 1.4792,-2.2016 3.44,-3.4056c1.9608,1.204 3.44,2.7176 3.44,3.4056v1.4448h-6.88zM106.8464,75.68h-0.4128c-3.6808,0 -6.6736,2.9928 -6.6736,6.6736c0,-3.6808 -2.9928,-6.6736 -6.6736,-6.6736h-0.4128c-3.6808,0 -6.6736,2.9928 -6.6736,6.6736c0,-3.6808 -2.9928,-6.6736 -6.6736,-6.6736h-0.4128c-3.6808,0 -6.6736,2.9928 -6.6736,6.6736c0,-3.6808 -2.9928,-6.6736 -6.6736,-6.6736h-0.4128c-1.1696,0 -2.2704,0.3096 -3.2336,0.86v-11.18h48.16v11.18c-0.9632,-0.5504 -2.064,-0.86 -3.2336,-0.86zM161.68,72.7904v-14.3104h3.44v-6.88h-3.612c-0.9976,-9.0128 -6.192,-11.9368 -13.588,-17.2c-7.396,5.2632 -12.5904,8.1872 -13.588,17.2h-3.612v6.88h3.44v14.3792c-4.73,1.5308 -6.88,6.192 -6.88,9.7008v10.32h41.28v-10.5264c0,-4.4376 -2.15,-7.9636 -6.88,-9.5632zM154.8,76.3336c0,-0.2408 0,-0.4816 -0.0344,-0.7224c-0.344,-3.8184 -3.268,-6.8112 -6.8456,-6.8112c-3.784,0 -6.88,3.3712 -6.88,7.5336v-17.8536h13.76zM44.72,99.76v58.48h-41.28v-58.48zM51.6,99.76v58.48h17.2v-13.76h34.4v13.76h17.2v-58.48zM110.08,137.6h-48.16c-1.4104,0 -2.236,-1.6168 -1.376,-2.752l8.256,-11.008h34.4l8.256,11.008c0.86,1.1352 0.0344,2.752 -1.376,2.752zM168.56,99.76v58.48h-41.28l0.2064,-58.48zM134.3664,96.32h-0.2064M37.84,73.2548v-14.7748h3.44v-6.45h-3.612c-0.9976,-9.0128 -6.192,-12.3668 -13.588,-17.63c-7.396,5.2632 -12.5904,8.6172 -13.588,17.63h-3.612v6.45h3.44v14.7748c-4.73,1.1352 -6.88,5.8824 -6.88,9.5288v10.0964h41.28v-10.0964c0,-3.7152 -2.15,-8.3936 -6.88,-9.5288zM30.96,76.7636c0,-0.2408 0,-0.4816 -0.0344,-0.7224c-0.344,-3.8184 -3.268,-7.2412 -6.8456,-7.2412c-3.784,0 -6.88,3.8012 -6.88,7.9636v-18.2836h13.76z"></path></g></g></svg>
						<h4>New Delhi</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
							<p>
								New Delhi is an urban of Delhi which serves as capital of India and seat branches of the Government of India.
							</p>
						</div>
						<a href="#">View</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="40" height="40"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g><g id="surface1"><path d="M86,48.375l-39.41667,34.04167l-25.08333,14.33333v53.75h129v-53.75l-24.36947,-13.61947z" fill="#e89419"></path><path d="M126.13053,79.5472l-40.13053,-34.75553l-39.41667,34.04167l-25.08333,14.33333v7.16667l25.08333,-14.33333l39.41667,-34.04167l40.13053,34.75553l24.36947,13.61947v-7.16667z" fill="#c62828"></path><path d="M125.41667,14.33333l-10.75,43h21.5z" fill="#c62828"></path><path d="M46.58333,14.33333l-10.75,43h21.5z" fill="#c62828"></path><path d="M114.66667,57.33333h21.5v93.16667h-21.5z" fill="#ffb74d"></path><path d="M35.83333,57.33333h21.5v93.16667h-21.5z" fill="#ffb74d"></path><path d="M100.33333,150.5h-28.66667v-32.25c0,-7.88053 6.4528,-14.33333 14.33333,-14.33333c7.88053,0 14.33333,6.4528 14.33333,14.33333z" fill="#805a26"></path><path d="M129,143.33333h-7.16667v-35.83333c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M50.16667,143.33333h-7.16667v-35.83333c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M129,89.58333h-7.16667v-21.5c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M50.16667,89.58333h-7.16667v-21.5c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M86,68.08333c-6.08887,0 -10.75,4.66113 -10.75,10.75c0,6.08887 4.66113,10.75 10.75,10.75c6.08887,0 10.75,-4.66113 10.75,-10.75c0,-6.08887 -4.66113,-10.75 -10.75,-10.75zM86,82.41667c-2.1556,0 -3.58333,-1.42773 -3.58333,-3.58333c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333c0,2.1556 -1.42773,3.58333 -3.58333,3.58333z" fill="#805a26"></path></g></g></g></svg>
						<h4>Hyderabad</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
							<p>
								Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do eiusmod tempor incididunt labore.
							</p>
						</div>
						<a href="#">View</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="40" height="40"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#e67e22"><path d="M48.10625,17.15297c-1.89722,0.02966 -3.41223,1.58976 -3.38625,3.48703v1.69312c-2.61251,2.22177 -6.88,7.56925 -6.88,18.94688v3.44v6.88c-1.24059,-0.01754 -2.39452,0.63425 -3.01993,1.7058c-0.62541,1.07155 -0.62541,2.39684 0,3.46839c0.62541,1.07155 1.77935,1.72335 3.01993,1.7058v13.76h-20.64v13.76h20.64v6.88h-34.4c-1.24059,-0.01754 -2.39452,0.63425 -3.01993,1.7058c-0.62541,1.07155 -0.62541,2.39684 0,3.46839c0.62541,1.07155 1.77935,1.72335 3.01993,1.7058v55.04h6.88v-55.04h6.88v55.04h6.88v-28.53453c0.02577,-5.58715 0.68341,-9.49671 6.88,-12.74547c6.5876,3.45376 6.92472,7.58176 6.88,13.76v27.52h6.88v-55.04h6.88v55.04h6.88h10.32v-41.28c0,-14.67848 17.2,-20.64 17.2,-20.64c0,0 17.2,5.96152 17.2,20.64v41.28h13.76h3.44v-55.04h6.88v55.04h6.88v-28.58828c0.02772,-5.55688 0.70412,-9.45382 6.88,-12.69172c6.19659,3.24876 6.85422,7.15832 6.88,12.74547v28.53453h6.88v-55.04h6.88v55.04h6.88v-55.04c1.24059,0.01754 2.39452,-0.63425 3.01993,-1.7058c0.62541,-1.07155 0.62541,-2.39684 0,-3.46839c-0.62541,-1.07155 -1.77935,-1.72335 -3.01993,-1.7058h-34.4v-6.88h20.64v-13.76h-20.64v-13.76c1.24059,0.01754 2.39452,-0.63425 3.01993,-1.7058c0.62541,-1.07155 0.62541,-2.39684 0,-3.46839c-0.62541,-1.07155 -1.77935,-1.72335 -3.01993,-1.7058v-10.32c0,-11.37813 -4.2689,-16.72528 -6.88,-18.94688v-1.69312c0.01273,-0.92983 -0.35149,-1.82522 -1.00967,-2.48214c-0.65819,-0.65692 -1.55427,-1.01942 -2.48408,-1.00489c-1.89722,0.02966 -3.41223,1.58976 -3.38625,3.48703v1.69312c-2.61251,2.22177 -6.88,7.56925 -6.88,18.94688v3.44v6.88h-55.04v-10.32c0,-11.37813 -4.2689,-16.72528 -6.88,-18.94688v-1.69312c0.01273,-0.92983 -0.35149,-1.82522 -1.00967,-2.48214c-0.65819,-0.65692 -1.55427,-1.01942 -2.48408,-1.00489zM44.72,44.72h6.88v6.88h-6.88zM120.4,44.72h6.88v6.88h-6.88zM44.72,58.48h6.88v13.76h-6.88zM120.4,58.48h6.88v13.76h-6.88zM5.16,72.24c-0.94944,0 -1.72,0.77056 -1.72,1.72v12.04h6.88v-13.76zM58.48,72.24h55.04v6.88h-55.04zM161.68,72.24v13.76h6.88v-12.04c0,-0.94944 -0.77056,-1.72 -1.72,-1.72zM44.72,79.12h6.88v13.76h-6.88zM120.4,79.12h6.88v13.76h-6.88zM30.96,130.72c0,0 -3.44,2.15 -3.44,6.88v17.2h6.88v-17.2c0,-4.73 -3.44,-6.88 -3.44,-6.88zM141.04,130.72c0,0 -3.44,2.15 -3.44,6.88v17.2h6.88v-17.2c0,-4.73 -3.44,-6.88 -3.44,-6.88z"></path></g></g></svg>
						<h4>Mumbai</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
							<p>
								Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do eiusmod tempor incididunt labore.
							</p>
						</div>
						<a href="#">View</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="feature-item">
					<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
						width="40" height="40"
						viewBox="0 0 172 172"
						style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g><g id="surface1"><path d="M150.5,57.33333h-21.5l10.75,-10.75z" fill="#c62828"></path><path d="M43,57.33333h-21.5l10.75,-10.75z" fill="#c62828"></path><path d="M114.66667,43h-57.33333l28.66667,-25.08333z" fill="#c62828"></path><path d="M129,57.33333h21.5v21.5h-21.5z" fill="#ffb74d"></path><path d="M57.33333,43h57.33333v35.83333h-57.33333z" fill="#e89419"></path><path d="M21.5,57.33333h21.5v21.5h-21.5z" fill="#ffb74d"></path><path d="M21.5,71.66667h129v75.25h-129z" fill="#ffb74d"></path><path d="M107.5,146.91667h-43v-21.5c0,-11.8278 9.6722,-21.5 21.5,-21.5c11.8278,0 21.5,9.6722 21.5,21.5z" fill="#e65100"></path><path d="M143.33333,89.58333h-7.16667v-25.08333c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M107.5,71.66667h-7.16667v-17.91667c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M71.66667,71.66667h-7.16667v-17.91667c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M93.16667,71.66667h-14.33333v-14.33333c0,-3.94726 3.2194,-7.16667 7.16667,-7.16667c3.94727,0 7.16667,3.2194 7.16667,7.16667z" fill="#805a26"></path><path d="M35.83333,89.58333h-7.16667v-25.08333c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M107.5,89.58333h-7.16667v-7.16667c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M71.66667,89.58333h-7.16667v-7.16667c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path><path d="M89.58333,89.58333h-7.16667v-7.16667c0,-2.1556 1.42774,-3.58333 3.58333,-3.58333c2.1556,0 3.58333,1.42774 3.58333,3.58333z" fill="#805a26"></path></g></g></g></svg>
						<h4>Kolkata</h4>
						<div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
							<p>
								Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do eiusmod tempor incididunt labore.
							</p>
						</div>
						<a href="#">View</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Feature Area -->


	<!-- Start Faculty Area -->
	<section class="faculty-area section-gap">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="section-title text-center">
						<h1>Meet the Students</h1>
						<p>
							
						</p>
					</div>
				</div>
			</div>
			<div class="row justify-content-center d-flex align-items-center">
				<div class="col-lg-3 col-md-6 col-sm-12 single-faculty">
					<div class="thumb d-flex justify-content-center">
						<img class="img-fluid" src="img/faculty/f1.jpg" alt="">
					</div>
					<div class="meta-text text-center">
						<h4>Ethel Davis</h4>
						<p class="designation">Sr. Faculty Data Science</p>
						<div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
							<p>
								Some Random text shoud be placed here for the working of the AICTE Feedback Portal
							</p>
						</div>
						<div class="align-items-center justify-content-center d-flex">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 single-faculty">
					<div class="thumb d-flex justify-content-center">
						<img class="img-fluid" src="img/faculty/f2.jpg" alt="">
					</div>
					<div class="meta-text text-center">
						<h4>Rodney Cooper</h4>
						<p class="designation">Sr. Faculty Data Science</p>
						<div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
							<p>
							Some Random text shoud be placed here for the working of the AICTE Feedback Portal
							</p>
						</div>
						<div class="align-items-center justify-content-center d-flex">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 single-faculty">
					<div class="thumb d-flex justify-content-center">
						<img class="img-fluid" src="img/faculty/f3.jpg" alt="">
					</div>
					<div class="meta-text text-center">
						<h4>Dora Walker</h4>
						<p class="designation">Sr. Faculty Data Science</p>
						<div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
							<p>
								Some Random text shoud be placed here for the working of the AICTE Feedback Portal
							</p>
						</div>
						<div class="align-items-center justify-content-center d-flex">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 single-faculty">
					<div class="thumb d-flex justify-content-center">
						<img class="img-fluid" src="img/faculty/f4.jpg" alt="">
					</div>
					<div class="meta-text text-center">
						<h4>Lena Keller</h4>
						<p class="designation">Sr. Faculty Data Science</p>
						<div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".7s">
							<p>
								Some Random text shoud be placed here for the working of the AICTE Feedback Portal
							</p>
						</div>
						<div class="align-items-center justify-content-center d-flex">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Faculty Area -->


	<!-- Start Testimonials Area -->
	<section class="testimonials-area section-gap">
		<div class="container">
			<div class="testi-slider owl-carousel" data-slider-id="1">
				<div class="item">
					<div class="testi-item">
						<img src="img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="owl-thumbs d-flex justify-content-center" data-slider-id="1">
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="img/testimonial/t1.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="img/testimonial/t2.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="img/testimonial/t3.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="img/testimonial/t4.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Testimonials Area -->


	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>