<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
	?>
	<!-- Start Banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						<?php echo $_GET['register']; ?>
					</h1>
					<div class="link-nav">
						<span class="box">
							<a href="index.php">Home </a>
							<i class="lnr lnr-arrow-right"></i>
							<a href="register.php"><?php echo $_GET['register']; ?></a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div>
	</section>
  <!-- End Banner Area -->
  <div id="messages"></div>
  <div id="allFormsContainer" class="container w-50 mt-4 mb-4">
    <h3 class="mb-30">Register <?php echo $_GET['register'] ?></h3>
    <?php if ($_GET['register'] === "Corporate Sector") { ?>
    <form>
    <div class="form-group">
        <label for="exampleInputEmail1">First Name</label>
        <input type="text" class="form-control" id="firstNameCorporateSector-txt" placeholder="Rohan">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Last Name</label>
        <input type="text" class="form-control" id="lastNameCorporateSector-txt" placeholder="Sharma">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="emailCorporateSector-txt" placeholder="example@example.com">
      </div>
      <!-- <div class="form-group">
        <label for="exampleInputEmail1">Contact No</label>
        <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
      </div> -->
      <div class="form-group">
      <label for="exampleInputEmail1">Select</label>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <label class="input-group-text" for="inputGroupSelect01">Options</label>
        </div>
        <select class="custom-select" id="select-CorporateSector">
          <option selected>Choose...</option>
          <option value="TIN">TIN</option>
          <option value="GST">GST</option>
          <option value="ROC">ROC</option>
          <option value="CIN">CIN</option>
          <option value="Association Registration">Association Registration</option>
        </select>
      </div>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">ID Number</label>
        <input type="text" class="form-control" id="idNumberCorporateSector-txt" placeholder="ID Number">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Organization</label>
        <input type="text" class="form-control" id="organizationCorporateSector-txt" placeholder="Organization">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="password-CorporateSector-pwd" placeholder="Password">
      </div>
      <button type="button" id="submit-corporateSector-btn" class="btn btn-block btn-primary">
        <i class="fas fa-thumbs-up"></i>
        Submit
      </button>
    </form>

    <?php } elseif ($_GET['register'] === "Govermental Department") { ?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="GovfirstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="GovlastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="Govemail-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
            <label for="exampleInputEmail1">Organization</label>
            <input type="text" class="form-control" id="GovOrganization-txt" placeholder="Organization">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="Govpassword-pwd" placeholder="Password">
          </div>
          <button type="button" id="submit-govermentSector-btn" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
        </form>
    <?php } elseif ($_GET['register'] === "For Student Registration") { ?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="StuFirstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="StuLastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="StuEmail-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
            <label for="institution">Select Institution</label>
            <?php include './includes/searchbar-institution.inc.php'; ?>
          </div>
          
          <!-- <div class="form-group">
          <label for="exampleInputEmail1">Select Institution</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Options</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
              <option selected>Choose...</option>
            </select>
          </div>
          </div> -->
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="Stu-pwd" placeholder="Password">
          </div>
          <button type="button" id="student-register-btn" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
        </form>
    <?php } elseif ($_GET['register'] === "Faculty") {?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="firstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="lastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
          <label for="exampleInputEmail1">Select</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Options</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
              <option selected>Choose...</option>
              <?php include './includes/select-organization.inc.php' ?>
            </select>
          </div>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
        </form>
    <?php } elseif ($_GET['register'] === "Institute") {?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="firstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="lastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
          <label for="exampleInputEmail1">Select</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Options</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
              <option selected>Choose...</option>
              <?php include './includes/select-organization.inc.php' ?>
            </select>
          </div>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
        </form>
    <?php } elseif ($_GET['register'] === "PUBLIC SECTOR") {?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="firstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="lastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
            <label for="exampleInputPassword1">Organization</label>
            <input type="password" class="form-control" id="organization-txt" placeholder="Organization">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
        </form>
    <?php } elseif ($_GET['register'] === "Public Sector") { ?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="firstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="lastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
          <label for="exampleInputEmail1">Select</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Options</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
              <option selected>Choose...</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">ID Number</label>
            <input type="text" class="form-control" id="idNumber-txt" placeholder="ID Number">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Organization</label>
            <input type="text" class="form-control" id="organization-txt" placeholder="Organization">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
    </form>
    <?php } elseif ($_GET['register'] === "Govermental") {?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="firstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="lastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
            <label for="exampleInputPassword1">Organization</label>
            <input type="password" class="form-control" id="organization-txt" placeholder="Organization">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
        </form>
    <?php } elseif ($_GET['register'] === "Skill Council") {?>
      <form>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control" id="firstName-txt" placeholder="Rohan">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control" id="lastName-txt" placeholder="Sharma">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email-txt" placeholder="example@example.com">
          </div>
          <!-- <div class="form-group">
            <label for="exampleInputEmail1">Contact No</label>
            <input type="email" class="form-control" id="contact-txt" placeholder="example@example.com">
          </div> -->
          <div class="form-group">
            <label for="exampleInputPassword1">Organization</label>
            <input type="password" class="form-control" id="organization-txt" placeholder="Organization">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-block btn-primary">
            <i class="fas fa-thumbs-up"></i>
            Submit
          </button>
        </form>  
    <?php } else { header("Location: ./register.php"); } ?>
  </div>
	<?php
		include './includes/footer.inc.php';
  ?>
  
</body>

</html>