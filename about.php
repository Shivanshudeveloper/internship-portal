<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
    ?>
    <!-- Start Banner Area -->
	<section class="banner-area relative">
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						About Us
					</h1>
					<div class="link-nav">
						<span class="box">
							<a href="index.php">Home </a>
							<i class="lnr lnr-arrow-right"></i>
							<a href="about.php">About Us</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<img src="img/rocket.png" alt="">
		</div>
	</section>
    <!-- End Banner Area -->
	<div class="container text-center mt-4 mb-4 w-100">
	<p class="font-italic">"Meet the individuals who bring unique perspectives, broad experience acquired through close collaborations, and deep insight to our work."</p>
	<div class="row">
		<img src="https://www.hindi.aicte-india.org/sites/default/files/leadership/Prof.%20Anil%20D.%20Sahasrabudhe_0.jpg" style="margin-left: 100px;" class="rounded w-25 img-fluid" >
		<img src="https://www.hindi.aicte-india.org/sites/default/files/leadership/poonia.JPG" class="rounded ml-4 w-25 img-fluid" >
		<img src="https://www.hindi.aicte-india.org/sites/default/files/leadership/Prof.%20Alok%20Prakash%20Mittal.jpg" class="rounded ml-4 w-25 img-fluid" >
	</div>
	<div class="row">
		<p class="mt-2" style="margin-left: 100px;">Prof. Anil D. Sahasrabudhe</p>
		<p class="mt-2" style="margin-left: 120px;">Dr. M.P. Poonia</p>
		<p class="mt-2" style="margin-left: 200px;">Prof. Anil D. Sahasrabudhe</p>
	</div>

	</div>


	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>