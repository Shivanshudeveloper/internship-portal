<?php
include './src/php/dbh.php';
$id = $_GET['id'];
$currentTimeinSeconds = time(); 
$date = date('Y-m-d', $currentTimeinSeconds);



$street_address = "Nelson Mandela Marg, Vasant Kunj, New Delhi, Delhi 110070";
$phone_number = "011 2613 1578";
$final_invoice_no = uniqid();

//-----------------------------------------------
//From here the INVOICE is getting generated
//-----------------------------------------------
//call the FPDF library
require('fpdf181/fpdf.php');
//A4 width : 219mm
//default margin : 10mm each side
//writable horizontal : 219-(10*2)=189mm
//create pdf object
$pdf = new FPDF('P', 'mm', 'A4');
//add new page
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 14);
//Cell(width , height , text , border , end line , [align] )
$pdf->Cell(130, 5, 'All India Council For Technical Education', 0, 0);
$pdf->Cell(59, 5, 'Internship', 0, 1);//end of line
//set font to arial, regular, 12pt
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(130, 5, '' . $street_address . '', 0, 0);
$pdf->Cell(30, 5, '', 0, 1);//end of line
//$pdf->Cell(130 ,5,'[City, Country, ZIP]',0,0);
$pdf->Cell(12, 5, 'Date.', 0, 0);
$pdf->Cell(30, 5, '' . $date . '', 0, 1);//end of line
$pdf->Cell(130, 5, 'Phone No. ' . $phone_number . '', 0, 0);
//$pdf->Cell(130 ,5,'Fax [+12345678]',0,0);
$pdf->Cell(28, 5, 'Internship No.', 0, 0);
$pdf->Cell(30, 5, '' . $final_invoice_no . '', 0, 1);

//make a dummy empty cell as a vertical spacer
$pdf->Cell(10, 10, '', 0, 1);//end of line
//billing address
// $pdf->Cell(10, 5, 'Bill To:', 0, 1);
//add dummy cell at beginning of each line for indentation

$pdf->Cell(10, 5, 'Full Name: ' . "Student" . '', 0, 1);
$pdf->Cell(10, 5, 'Email: ' . "student@gmail.com" . '', 0, 1);
//make a dummy empty cell as a vertical spacer
$pdf->Cell(189, 10, '', 0, 1);//end of line
//invoice contents
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(60, 5, 'User', 1, 0);
$pdf->Cell(20, 5, 'ID.', 1, 0);
$pdf->Cell(40, 5, 'Phone.', 1, 0);
$pdf->Cell(40, 5, 'Sector.', 1, 0);
$pdf->Cell(30, 5, 'Stipend', 1, 1);//end of line
$pdf->SetFont('Arial', '', 12);
$email = '';
$fullName = '';
$phoneNo = '';
$sql = "SELECT * FROM internship_applied WHERE internship_id = '$id';";
$result = mysqli_query($conn, $sql);
$resultChk = mysqli_num_rows($result);
if ($resultChk < 1) {

} else {
    while ($row = mysqli_fetch_assoc($result)) {
        $email = $row['user_email'];
    }
}
$sql = "SELECT * FROM student WHERE email = '$email';";
$result = mysqli_query($conn, $sql);
$resultChk = mysqli_num_rows($result);
if ($resultChk < 1) {

} else {
    while ($row = mysqli_fetch_assoc($result)) {
        $fullName = $row['first_name'].' '.$row['last_name'];
        $phoneNo = $row['phone'];
    }
}

$sql = "SELECT * FROM internships WHERE id = '$id';";
$result = mysqli_query($conn, $sql);
$resultChk = mysqli_num_rows($result);
if ($resultChk < 1) {
    echo 'No Internship Found';
} else {
    while ($row = mysqli_fetch_assoc($result)) {
        $pdf->Cell(60, 5, $fullName, 1, 0);
        $pdf->Cell(20, 5, $id, 1, 0);
        $pdf->Cell(40, 5, $phoneNo, 1, 0);
        $pdf->Cell(40, 5, $row['sector'], 1, 0);
        $pdf->Cell(30, 5, $row['stipend'], 1, 1);//end of line
    }
}



//summary


// $pdf->Cell(140, 5, '', 0, 0);
// $pdf->Cell(20, 5, 'SGST', 0, 0);
// $pdf->Cell(30, 5, '9.0%', 1, 1, 'R');//end of line

//output the result
$pdf->Output();
?>