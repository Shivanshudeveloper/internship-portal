<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
        include './includes/user-navigation.inc.php';
    ?>
    <section class="feature-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="section-title text-center">
						<h1>
                            <i class="fas fa-chart-line"></i>
							 Analytics of Internships
                        </h1>
					</div>
				</div>
			</div>
			<div class="container">
                <h3>Internships</h3>
                <?php
                    include './src/php/dbh.php';
                    // Getting User Email from session
                    session_start();
                    $userEmail = $_SESSION['useremail'];
                    $sql = "SELECT * FROM internships WHERE internship_of = '$userEmail';";
                    $result = mysqli_query($conn, $sql);
                    $resultChk = mysqli_num_rows($result);
                    if ($resultChk < 1) {
                        echo "No Internships Found!";
                    } else {
                        echo '
                        <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Duration</th>
                            <th scope="col">Stipend</th>
                            <th scope="col">Posted On</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                        ';
                        $count = 0;
                        while ($row = mysqli_fetch_assoc($result)) {
                            $count++;
                            echo '
                            <tr>
                                <form action="./src/php/main.php" method="POST">
                                <th scope="row">'.$count.'</th>
                                <td>'.$row['title'].'</td>
                                <td>'.$row['maximum_duration'].'</td>
                                <td>'.$row['stipend'].'</td>
                                <td>'.$row['post_on'].'</td>
                                <td>
                                <a href="#" onclick="viewInternship('.$row['id'].')">
                                    View
                                </a>
                                </td>
                                <td>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fas fa-file-excel"></i>
                                    Download Excel
                                </button>
                                </td>
                            </tr>
                            ';
                        }
                        echo '
                        </tbody>
                        </table>
                        ';
                    }
                ?>
            </div>
		</div>
	</section>

	<?php
		include './includes/footer.inc.php';
	?>
</body>

</html>