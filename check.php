<!-- Header Included -->
<?php
	include './includes/header.inc.php';
?>
<body>
	<?php
		include './includes/navigation.inc.php';
    ?>
    <?php
        include './src/php/dbh.php';
        if (isset($_GET['check'])) {
            $email = $_GET['email'];
            // Saving user email in session
            session_start();
            $_SESSION['useremail'] = $email;
            $sql = "SELECT * FROM users WHERE email = '$email';";
            $result = mysqli_query($conn, $sql);
            $resultChk = mysqli_num_rows($result);
            if ($resultChk < 1) {
                header("Location: ../../login.php?message=UserNotFound");
                exit();
            } else {
                while ($row = mysqli_fetch_assoc($result)) {
                    $loginType = $row['type'];
                    $uid = $row['uid'];
                    // Checking for the user type
                    if ($loginType === "STUDENT") {
                        $sql = "SELECT * FROM student WHERE uid = '$uid';";
                        $result = mysqli_query($conn, $sql);
                        $resultChk = mysqli_num_rows($result);
                        if ($resultChk < 1) {
                            echo 'No User Found';
                        } else {
                            while ($row = mysqli_fetch_assoc($result)) {
                                $firstTime = $row['first_time'];
                                if ($firstTime == 1) {
                                    header("Location: ./student-info.php?register=Student&email=".$email);
                                } else {
                                    header("Location: ./dashboard.php?type=student&page=dashboard&email=".$email);
                                }
                            }
                        }
                    } else if ($loginType === "GOVERMENT ORGANIZATION") {
                        $sql = "SELECT * FROM gov_organization WHERE uid = '$uid';";
                        $result = mysqli_query($conn, $sql);
                        $resultChk = mysqli_num_rows($result);
                        if ($resultChk < 1) {
                            echo 'No User Found';
                        } else {
                            while ($row = mysqli_fetch_assoc($result)) {
                                $firstTime = $row['first_time'];
                                if ($firstTime == 1) {
                                    header("Location: ./govdepartment-info.php?register=Student&email=".$email);
                                } else {
                                    header("Location: ./dashboard.php?type=govermentdepartment&page=dashboard&email=".$email);
                                }
                            }
                        }
                    }
                }
            }
        }
    ?>    
	<?php
		include './includes/footer.inc.php';
    ?>
    <script>
        var pageURL = window.location.href;
            url = new URL(pageURL);
            page = url.searchParams.get("register");
        if (page === "student") {
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    var email = sessionStorage.getItem("emailRegister");
                    console.log(email)
                    $.post("./src/php/main.php", {
                        phoneNumber: user.phoneNumber,
                        email: email,
                        phoneNumberVerifyStudent: true
                    }).then(() => {
                        console.log("Done")
                        firebase.auth().signOut().then(function () {
                            window.location.href = "login.php"
                        }).catch(function (error) {
                            console.error(error)
                        });
                    })
                }
            });
        } else if (page === "corporate") {
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    var email = sessionStorage.getItem("emailRegister");
                    console.log(email)
                    $.post("./src/php/main.php", {
                        phoneNumber: user.phoneNumber,
                        email: email,
                        phoneNumberVerifyCorporate: true
                    }).then(() => {
                        firebase.auth().signOut().then(function () {
                            window.location.href = "login.php"
                        }).catch(function (error) {
                            console.error(error)
                        });
                    })
                }
            });
        } else if (page === "Goverment Organization") {
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    var email = sessionStorage.getItem("emailRegister");
                    console.log(email)
                    $.post("./src/php/main.php", {
                        phoneNumber: user.phoneNumber,
                        email: email,
                        phoneNumberVerifyGovOrganization: true
                    }).then(() => {
                        firebase.auth().signOut().then(function () {
                            window.location.href = "login.php"
                        }).catch(function (error) {
                            console.error(error)
                        });
                    })
                }
            });
        }
        
    </script>
</body>

</html>