<header id="header">
		<div class="container">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="index.php"><img src="./img/aicte/logo.png" width="110%" alt="LOGO" title="" /></a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li class="menu-active"><a href="index.php">Home</a></li>
						<li><a href="about.php">About</a></li>
						<li><a href="#">Courses</a></li>
						<li class="menu-has-children"><a href="#">Internships</a>
							<ul>
								<li><a href="#">Internships 1</a></li>
							</ul>
						</li>
						<li><a href="./login.php">Login</a></li>
						<li><a href="./register.php">Register</a></li>
					</ul>
				</nav><!-- #nav-menu-container -->
			</div>
		</div>
	</header>