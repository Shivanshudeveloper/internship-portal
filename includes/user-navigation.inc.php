<header id="header">
		<div class="container">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="index.html"><img src="./img/aicte/logo.png" width="110%" alt="LOGO" title="" /></a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li><div id="dashboardHome"></div></li>
						<li><div id="internshipDetails"></div></li>
						<li class="menu-has-children"><a class="text-success" href="#">Internships</a>
							<ul>
								<div id="internshipMore"></div>
							</ul>
						</li>
						<li><div id="userName"></div></li>
						<li><a href="#" class="text-danger" id="logout">Logout</a></li>
					</ul>
				</nav><!-- #nav-menu-container -->
			</div>
		</div>
	</header>