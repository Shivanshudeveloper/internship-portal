<!-- Start Footer Area -->
<footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 single-footer-widget">
					<h4>Student Internships</h4>
					<ul>
						<li><a href="SearchAllInternship.aspx" title=""> All Internships</a></li>
						<li><a href="SearchLOcation.aspx" title=""> Internships in India</a></li>
						<li><a href="SearchInternationalLocation.aspx" title=""> International Internships</a></li>
						<li><a href="SearchGovernment.aspx" id="A4">Internships in Government</a></li>
						<li><a href="SearchCompany.aspx" id="A2">Internships by Company</a></li>
						<li><a href="SearchCategory.aspx" id="A3">Internships by Category</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-6 single-footer-widget">
					<h4>Faculty Internships</h4>
					<ul>
						<li><a href="SearchFacultyInternship.aspx" title=""> All Internships</a></li>
						<li><a href="SearchFacLocation.aspx" title=""> Internships in India</a></li>
						<li><a href="SearchInternationalLocation.aspx" title=""> International Internships</a></li>
                        <li><a href="SearchFacGovernment.aspx" id="A12">Internships in Government</a></li>
                        <li><a href="SearchFacCompany.aspx" id="A18">Internships by Company</a></li>
                        <li><a href="SearchFacCategory.aspx" id="A19">Internships by Category</a></li>
					</ul>
				</div>
				<div class="col-lg-2 col-md-6 single-footer-widget">
					<h4>Quick Links</h4>
					<ul>
						<li><a href="#" title="">About Us</a></li>
						<li><a href="#" title="">News</a></li>
						<li><a href="#" title="">Terms of Serice</a></li>
                        <li><a href="#" title="">Privacy</a></li>
                        <li><a href="ContactUs.aspx" title="">Contact Us</a></li>
					</ul>
				</div>
				<div class="col-lg-4 col-md-6 single-footer-widget">
					<h4>For Internship Policy Related Queries, Contact</h4>
                    <p>All India Council for Technical Education<br>
                    Nelson Mandela Marg, Vasanth Kunj, New Delhi<br>
                    +91 11 26131578 <br>
                    <a href="mailto:aicteinternshippolicy@aicte-india.org">aicteinternshippolicy@aicte-india.org</a>
				</div>
			</div>
			<div class="footer-bottom row align-items-center">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Design and Developed By  &copy;<script>document.write(new Date().getFullYear());</script><a href="https://www.geu.ac.in">Graphic Era Deemed to be University</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<div class="col-lg-4 col-md-12 footer-social">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-dribbble"></i></a>
					<a href="#"><i class="fa fa-behance"></i></a>
				</div>
			</div>
		</div>
	</footer>
	<!-- End Footer Area -->

	<!-- ####################### Start Scroll to Top Area ####################### -->
	<div id="back-top">
		<a title="Go to Top" href="#"></a>
	</div>
	<!-- ####################### End Scroll to Top Area ####################### -->

	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	 crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="js/easing.min.js"></script>
	<script src="js/hoverIntent.js"></script>
	<script src="js/superfish.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/owl-carousel-thumb.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/parallax.min.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/jquery.counterup.min.js"></script>
	<script src="js/mail-script.js"></script>
	<script src="js/main.js"></script>
	<script src="js/notify.js"></script>
	<script src="src/js/firebase-config.js"></script>
	<script src="src/js/main.js"></script>